$(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    }
  });
});

$(document).on('change', '.show-preview', function(){

		$("#preview").empty();
		
	var i         = 2; // 2 weil hidden feld festgesetzt
	var selected  = $(this).find('option:selected');
	var heads     = selected.data("amountheads");
	var component = selected.data("title");
	var div       = $(document).find('#heads-forms').removeClass('hidden');
		div.find('input[type=text],textarea').attr("required", true);

	if (heads <= 0) { 
		div.addClass('hidden');
		div.find('input[type=text],textarea').attr("required", false);
	}

	if(heads == -1){
		$(document).find('.article-enable-form').attr("disabled", true);
		$("#preview-info").addClass('hidden');
	}else{
		$(document).find('.article-enable-form').attr("disabled", false);
		$("#preview-info").removeClass('hidden');
	}

	$("#preview").append('<div class="container text-center"><div class="col-sm-2 col-sm-offset-5"><div class="loader"></div></div></div>');

	$(document).find('.heads-forms').remove();

	while (i <= heads) {
		div.clone().appendTo('.heads-input').addClass('heads-forms');
		i = i + 1;
	}
	
    $("#preview").load('/templates/' + component + '.php', function () {
		$(document).find('.loader').remove();
    });
});

$(document).on('change', '.guestbook-status', function(){

	$(this).addClass('loader');
	
	var selected  = $(this).find('option:selected');
	var id = selected.data("id");
	var status = selected.val();

    $.ajax({
          method: 'POST',
          url: "/backend/guestbook/update/"+ id,
		  data: { 
	        'status': status, 
	      },
          async: true,
          success: function (data) {
          	if (data == 'true'){
                location.reload();
            }else if (data == 'false'){
                alert('Es ist leider etwas schief gelaufen. Bitte lade die Seite neu und versuche es dann nochmal. Wenn es dann immer noch nicht funktioniert kontaktiere den Admin!');
            }
          }
        });
	
});

$(document).on('click', '.change-position', function(){

	$(this).removeClass('fa-arrow-up');
	$(this).removeClass('fa-arrow-down');
	$(this).addClass('loader');

	var position = $(this).data('new-position');
	var id       = $(this).data('id');

    $.ajax({
          url: "/backend/article/edit/"+ id + "/"+ position,
          async: true,
          method: 'GET',
          success: function (data) {
          	if (data == 'true'){
                location.reload();
            }else if (data == 'false'){
                alert('Es ist leider etwas schief gelaufen. Bitte lade die Seite neu und versuche es dann nochmal. Wenn es dann immer noch nicht funktioniert kontaktiere den Admin!');
            }
          }
        });

});
$(document).on('click', '.avatar-pick', function(){

	$(this).parent().find('img').removeClass('AvatarSelected');
	$(this).find('img').addClass('AvatarSelected');

	var id       = $(this).data('id');
	console.log(id);
	var div       = $(document).find('#avatar').val(id);

});

$(document).ready(function(){
	$('.slick-avatar').slick({
	  dots: true,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 5,
	  slidesToScroll: 5,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 4,
	        slidesToScroll: 4,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
});