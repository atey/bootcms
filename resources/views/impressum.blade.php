@extends('layouts.master')

@section('head')
    <li class="active"><a href="/">Home</a></li>
    @if(Auth::user())
    <li><a href="/backend"><i class="fa fa-wrench" aria-hidden="true"></i> Baukasten</a></li>
    @endif
@endsection

@section('content')
    <section class="about section-padding" 
    @if($articles->count() > 0) id="{{ $articles->first()->title_nav }}" @else id="contact" @endif
    >
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="section-title">
                  <h2 class="head-title lg-line">{{ $info->firm }}</h2>
                  <hr class="botm-line">
                  <p class="sec-para">
                        {{ $info->firm }}<br>
                        {{ $info->address}} {{ $info->address_number}}<br>
                        {{ $info->zip_code}} {{ $info->city}}<br>
                        <br>
                        {{ $info->phone }}<br>
                        {{ $info->email }}<br>
                  </p>
                  <p class="sec-para">
                        <a href="https://www.vecteezy.com">Vector Illustration by Vecteezy!</a><br>
                  </p>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
               <div style="visibility: visible;" class="col-sm-9 more-features-box">
                  <div class="more-features-box-text">
                    <div class="more-features-box-text-icon"> <i class="fa fa-angle-right" aria-hidden="true"></i> </div>
                    <div class="more-features-box-text-description">
                        <h3>Impressum</h3>
                        <p>{!! nl2br(e($info->impressum)) !!}</p>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
 

@endsection

