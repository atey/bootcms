@extends('layouts.backend')

@php
 $articlekey = 0;
@endphp

@section('content')
	<section id="contact" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="ser-title">Sektion bearbeiten</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-4 col-sm-4">
			      <h3>Kurzbeschreibung</h3>
			      <div class="space"></div>
			      <p>Nachdem Du das Design ausgewählt hast erscheint eine Vorschau der Sektion. Zusätzlich erscheinen weitere Felder die Du befüllen kannst. Diese Felder entsprechen den in der Vorschau angezeigten Bereichen wo Du dann deine persönlichen Daten angeben kannst.</p>
			    </div>

				<form action="/backend/article/update/{{$article->id}}" method="post" role="form" class="contactForm">
        			<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="col-md-8 col-sm-8 marb20">
						<div class="contact-info">
							<h3 class="cnt-ttl">Bearbeite hier deine Sektion</h3>
							<div class="space"></div>
                        	{{ method_field('PATCH') }}

						    <div class="form-group">
						    	<label>Sektion</label>
	                            <select  name="section_id" class="form-control br-radius-zero show-preview" required="required" disabled="disabled">
	                            	<option value="0" data-amountheads="0">Design</option>
	                            	@foreach($sections as $section)
		                            	@if($section->id == $article->section_id)
		                            	<option value="{{$section->id}}" data-amountheads="{{$section->amount_heads}}" selected="selected">{{$section->description}}</option>
		                            	@endif
	                            	<option value="{{$section->id}}" data-amountheads="{{$section->amount_heads}}" data-title="{{$section->title}}">{{$section->description}}</option>
	                            	@endforeach
	                            </select>
                            </div>
                            
						    <div class="form-group">
							    	<label>Überschrift für die Navigation</label>
							    	<br>
							    	<i class="fa fa-info-circle" style="font-size: 12.5px" aria-hidden="true"></i><small style="font-size: 12.5px"> Wird bei kleineren Sektionen nicht angezeigt (z.B. "Zitat")</small>
                                <input type="text" name="title_nav" class="form-control br-radius-zero" value="{{  $article->title_nav }}" />
                            </div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row">
					<hr>
					<br>
					<h2 class="text-center">Live Vorschau</h2>
					<div id="preview-info" class="text-center" style="font-size: 13px;">
						<br>
						<i class="fa fa-info-circle" aria-hidden="true"></i> Die Live Vorschau wird nach jedem Speichern aktualisiert
						<br>
						<i class="fa fa-info-circle" aria-hidden="true"></i> Die Texte kannst Du weiter unten bearbeiten
					</div>
					<br>
                	@php
                	$title = $article->section->title;
                	@endphp
                	@include("layouts.sections.$title")
					<hr>
					<h2 class="text-center">Texte bearbeiten</h2>
					<br>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				    	<label>Haupt-Überschrift und Titel für Sektion</label>
					    <div class="form-group">
                            <input type="text" name="title" class="form-control br-radius-zero" value="{{  $article->title }}"/>
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
					    	<label>Text unter Haupt-Überschrift</label>
                            <textarea class="form-control br-radius-zero" name="title_text" rows="5" data-rule="required">{{  $article->title_text }}</textarea>
                            <div class="validation"></div>
                        </div>
					</div>

					<div class="col-md-8 col-sm-12 marb20">
                        <div class="row heads-input">
                        @foreach($article->records as $record)
                        	<div id="heads-forms" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							    <div class="form-group">
					    			<label>Überschrift Bereich</label>
							    	<input type="hidden" name="content_ids[]" id="input" class="form-control" value="{{ $record->id }}">
	                                <input type="text" name="content_heads[]" class="form-control br-radius-zero" value="{{ $record->content_head }}" required="required"/>
	                            </div>
	                            <div class="form-group">
					    			<label>Text unter Überschrift Bereich</label>
	                                <textarea class="form-control br-radius-zero" name="content_texts[]" rows="5" data-rule="required" required="required">{{ $record->content_text }}</textarea>
	                            </div>
                        	</div>
                        
                        @endforeach
                        </div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-action">
							<br>
							<button type="submit" class="btn btn-form pull-right">Sektion Speichern</button>
						</div>
					</div>
				</form>
				@include('backend.article.components.existingTable')
			</div>
		</div>
	</section>
@endsection