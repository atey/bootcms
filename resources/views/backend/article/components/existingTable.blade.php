<div class="col-md-12">
	<hr>
	<h2 class="ser-title">Bereits vorhandene Sektionen</h2>
	<hr class="botm-line">
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Titel</th>
					<th>Titel Navi</th>
					<th>Template</th>
					<th>Überschriften</th>
					<th>Position</th>
					<th>Position ändern</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($articles as $oarticle)
				<tr>
					<td>{{$oarticle->title}}</td>
					<td>{{$oarticle->title_nav}}</td>
					<td>{{$oarticle->section->description}}</td>
					<td>
						@foreach($oarticle->records as $record)
							{{ $record->content_head }},
						@endforeach
					</td>
					<td>
						{{ $oarticle->position }}
					</td>
					<td>
						@if($oarticle->position - 1 != 0)
						<button type="button" data-id="{{ $oarticle->id}}" data-new-position="{{ $oarticle->position - 1 }}" class="fa fa-arrow-up change-position"></button>
						@endif
						@if($articles->count() != $oarticle->position)
						<button type="button" data-id="{{ $oarticle->id}}" data-new-position="{{ $oarticle->position + 1 }}" class="fa fa-arrow-down change-position"></button>
						@endif
					</td>
					<td>
						<a href="/backend/article/edit/{{$oarticle->id}}"><span class="fa fa-pencil btn-action"></span></a>
					</td>
					<td>
						<a data-toggle="modal" href='#modal-{{ $oarticle->id }}'><span class="fa fa-trash btn-action"></span></a>
						<div class="modal fade" id="modal-{{ $oarticle->id }}">
							<div class="modal-dialog">
								<div class="modal-content text-center">
									<div class="modal-header">
										<h4 class="modal-title">Diesen Eintrag wirklich löschen ?</h4>

										<a href="/backend/article/delete/{{$oarticle->id}}"><i class="fa fa-check pull-left btn-action" title="endgültig löschen" aria-hidden="true"></i></a>

										<span><i class="fa fa-ban close btn-action" title="nicht löschen" data-dismiss="modal" aria-hidden="true"></i></span>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>