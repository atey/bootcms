@extends('layouts.backend')

@section('content')
	<!--contact-->
	<section id="contact" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="ser-title">1. Was ist der Kopfbereich?</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-8 col-md-offset-2">
					<div class="contact-info faqs_head">
						<p>Mit Kopfbereich ist der oberste Bereich deiner Homepage gemeint. <br><br>

						<div class="panel panel-warning">
							<div class="panel-heading">
								<h3 class="panel-title">Der Kopfbereich besteht meist aus</h3>
							</div>
							<div class="panel-body">
								- einer <a href="#navigation">Navigationsleiste</a> <br>
								- einem Logo <br>
								- einem Hintergrundbild <br>
							</div>
						</div>
					  und einer Begrüßung oder Überschrift. Der Aufbau deines Kopfbereiches hängt auch vom gewählten Design ab.</p>
					</div>
				</div>
			</div>
			<div class="space"></div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="ser-title">2. Wozu brauche ich ein Hintergrundbild im Kopfbereich?</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-8 col-md-offset-2">
					<div class="contact-info faqs_head">
						Das Hintergrundbild im Kopfbereich deiner Homepage sagt „Hallo und Willkommen“ und ist außerdem
						sowas wie die Visitenkarte Deiner Firma oder Person. Das Foto sollte also möglichst auf den ersten
						Blick verdeutlichen, was die Besucher auf der Homepage finden werden. Solltest Du kein
						eigenes Foto zur Hand haben, das eine professionelle Visitenkarte deiner Homepage
						ausmacht, kannst Du im Internet zahlreiche kosten- und lizenzfreie Fotos finden, die Du in
						deinem Baukasten hochladen kannst. Wichtig ist nur, dass diese wirklich als kosten- und
						lizenzfrei gekennzeichnet sind.

					</div>
				</div>
			</div>
			<div class="space"></div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="ser-title">3. Worauf solltest Du beim Deinem Logo achten?</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-8 col-md-offset-2">
					<div class="contact-info faqs_head">
						Der Hintergrund deines Logos sollte möglichst transparent sein damit kein unschöner
						Rahmen oder Rand auf deiner Homepage zu sehen ist. Transparenter Hintergrund
						wird in der Bildansicht in grau/weiß kariert angezeigt.
					</div>
				</div>
			</div>
			<div class="space"></div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="ser-title">4. Wofür sind Keywords wichtig?</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-8 col-md-offset-2">
					<div class="contact-info faqs_head">
						Wenn möglichst viele Besucher deine Homepage in Suchmaschinen wie zum Beispiel
						Google finden sollen, kannst Du in den Hintergrundeinstellungen deiner Homepage
						bestimmte Schlüsselwörter definieren, (engl. Key Words) die potenzielle Besucher
						Deiner Homepage in Suchmaschinen eingeben könnten um Seiten Deiner Art zu finden. 
						
							<br><br><br>
						<div class="panel panel-warning">
							<div class="panel-heading">
								<h3 class="panel-title">Bist Du zum Beispiel Inhaber/in eines Friseursalons in Köln werden potenzielle 
						Besucher deiner Homepage so etwas wie</h3>
							</div>
							<div class="panel-body">
								<i class="fa fa-key" aria-hidden="true"></i>
								„Friseur“, „Köln“, „Friseursalon“, „Haare“, „schneiden“ oder „Haarschnitt“
							</div>
						</div>
						<br>
						in Suchmaschinen eingeben, um eben einen Friseur in Köln zu finden. Wenn Du Deine
						eigenen Key Words für Deine Homepage definierst, wird Dein Webauftritt in
						Suchmaschinen weiter oben gelistet und so besser gefunden werden.
					</div>
				</div>
			</div>
			<div class="space"></div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="ser-title">5. Was bei der Eingabe der Kontaktdaten zu beachten ist</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-8 col-md-offset-2">
					<div class="contact-info faqs_head">
						Die Kontaktdaten auf Deiner Homepage sind besonders wichtig und sollten immer aktuell
						sein. Du kannst sie in deinem Baukasten ganz einfach jederzeit aktualisieren. Die
						Informationen, die Du hier angibst, werden auch automatisch zu Deinem Impressum hinzugefügt. Im Social Media Bereich Deines Baukastens kannst Du außerdem deine Social Media
						Accounts wie zum Beispiel Deinen Facebook- oder Twitter-Account auf Deiner Homepage
						verlinken. So können sich Deine Homepagebesucher mit Dir vernetzen und Du kannst Dein
						Netzwerk in sozialen Medien ausbauen.
					</div>
				</div>
			</div>
			<div class="space"></div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="ser-title">6. Was ist bei Überschriften und Texten zu beachten</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-8 col-md-offset-2">
					<div class="contact-info faqs_head">
						Überschriften solltest Du möglichst kurz und präzise halten, damit sich die Besucher Deiner
						Homepage leicht zurechtfinden können. Das gilt insbesondere für Überschriften von neuen
						Sektionen, da diese Überschriften automatisch zu Überschriften in Deiner Navigationsleiste
						werden. <br>
						Allgemein solltest Du Überschriften und Texte in der Länge möglichst am ausgewählten Design Deines Homepagebaukastens orientieren, so wirkt der Aufbau deiner Homepage später besonders
						professionell. Um deine eigenen Texte im Homepagebaukasten einzufügen, hilft Dir die Vorschau dabei, eine geeignete Sektion zu finden. 
					</div>
				</div>
			</div>
			<div class="space"></div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="ser-title">7. Was ist eine Sektion?</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-8 col-md-offset-2">
					<div class="contact-info faqs_head">
						Sektionen sind einzelne Bereiche auf Deiner Homepage, die Du ganz einfach mit Texten oder
						Bildern füllen kannst. Du wählst zuerst ein Design für Deine erste Sektion und füllst dann die
						leeren Felder gemäß der angezeigten Vorlage aus. Die Vorschau zeigt Dir, wie die jeweilige
						Sektion mit Deinen eigenen Inhalten auf Deiner Homepage aussehen wird. Wenn Du mit Deinem Homepagebaukasten
						startest, ist Deine Homepage also noch leer und beinhaltet noch keine Texte. In der Gestaltung der Sektionen sind Deiner Fantasie keine Grenzen gesetzt! Die Reihenfolge der Sektionen kannst Du jederzeit
						über die Pfeile ändern. 
						<br><br><br>
						<ul class="list-group">
							<li class="list-group-item list-group-item-warning">
								<div class="row">
									<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
										<i class="fa fa-exclamation-circle" aria-hidden="true" style="font-size: 25px"></i>
									</div>
									<div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
										Überschriften der einzelnen Sektionen werden automatisch als Überschriften und Navigationspunkte in Deiner Navigationsleiste angezeigt.
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div id="navigation" class="space"></div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="ser-title">8. Was bedeutet Navigationsleiste und was passiert dort?</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-8 col-md-offset-2">
					<div class="contact-info faqs_head">
						In dieser Leiste werden die Menüpunkte deiner Webseite als Überschriften angezeigt. Klicken
						deine Homepage-Besucher auf diese Überschriften, scrollt deine Webseite automatisch auf
						den ausgewählten Bereich, den Du vorher als neue Sektion im Baukasten angelegt hast.
					</div>
				</div>
			</div>
			<div class="space"></div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="ser-title">9. Homepagebaukasten</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-8 col-md-offset-2">
					<div class="contact-info faqs_head">
						Das Wort Homepagebaukasten ist eine Zusammensetzung der Wörter Homepage und Baukasten. <br><br>

						<div class="panel panel-warning">
							<div class="panel-heading">
								<h3 class="panel-title">
									<b>- Homepage, Home-Page, die</b><br>
									<i>Substantiv feminin, (dt. Internetseite, Webseite)</i>
								</h3>
							</div>
							<div class="panel-body">
								1. über das Internet als grafische Darstellung abrufbare Datei, die als Ausgangspunkt zu
								den angebotenen Informationen einer Person, Firma oder Institution dient; Leitseite,
								Startseite <br><br>
								2. Gesamtheit der Dateien einer Person, Firma oder Institution, die von der Homepage
								erreichbar sind
							</div>
						</div>
						<div class="panel panel-warning">
							<div class="panel-heading">
								<h3 class="panel-title">
									<b>- Baukasten, der</b><br>
									<i>Substantiv maskulin</i>
								</h3>
							</div>
							<div class="panel-body">
								1. Kasten mit Bauklötzen als Kinderspielzeug
							</div>
						</div>

						<b></b><br>
						Naja, stimmt hier nicht ganz, aber der Homepagebaukasten ist so konzipiert, dass man ohne
						Programmier- oder besondere Internetkenntnisse in wenigen Minuten eine professionelle
						Webseite einfach selber bauen kann. Wirklich kinderleicht! Dahinter steckt ein sogenanntes
						CMS (Content Management System), dass es dem Anwender erlaubt, über
						benutzerfreundliche Eingabemasken Texte und Dateien in Webdesign-Templates
						einzufügen. Aber Stop: All dies würde hier zu weit führen – der Homepagebaukasten ist nun
						mal einzig und allein für Anwender, die eben keine Programmierkenntnisse haben. <br><br>
						Dieser Homepagebaukasten ist übrigens kein Massenprodukt eines Großkonzerns, sondern in
						liebevoller Handarbeit durch einen Einzelunternehmer und passionierten Web-Entwickler
						entstanden.

					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/ contact-->

@endsection