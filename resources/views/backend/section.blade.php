@extends('layouts.backend')

@section('content')
	<!--contact-->
	<section id="contact" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>title</th>
							</tr>
						</thead>
						<tbody>
							@foreach($sections as $section)
							<tr>
								<td>{{$section->title}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>

				<div class="col-md-12">
					<h2 class="ser-title">Sektion anlegen</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-4 col-sm-4">
			      <h3>Kurzbeschreibung</h3>
			      <div class="space"></div>
			      <p>Nachdem Du die Artikel-Art ausgewählt hast erscheint eine Vorschau des Artikels. Zusätzlich erscheinen weitere Felder die Du befüllen kannst. Diese Felder entsprechen den in der Vorschau angezeigten Felder wo Du dann deine persönlichen Daten angeben kannst.</p>
			    </div>
				<div class="col-md-8 col-sm-8 marb20">
					<div class="contact-info">
							<h3 class="cnt-ttl">Lege hier deine Sektion an.</h3>
							<div class="space"></div>
							<form action="/backend/section/store" method="post" role="form" class="contactForm">
                    			<input type="hidden" name="_token" value="{{ csrf_token() }}">

							    <div class="form-group">
	                                <input type="text" name="title" class="form-control br-radius-zero" placeholder="Sektion Überschrift und Filename"   />
	                                <div class="validation"></div>
	                            </div>    

							    <div class="form-group">
	                                <input type="number" name="amount_heads" class="form-control br-radius-zero" placeholder="Anzahl Überschiften"   min="1"/>
	                                <div class="validation"></div>
	                            </div>    
							    <div class="form-group">
	                                <input type="number" name="amount_links" class="form-control br-radius-zero" placeholder="Anzahl Links"   min="1"/>
	                                <div class="validation"></div>
	                            </div>  
							    <div class="form-group">
		                            <select  name="title_text_active" class="form-control br-radius-zero show-preview" required="required">
		                            	<option value="0">aktiviert</option>
		                            	<option value="1">deaktiviert</option>
		                            </select>
	                                <div class="validation"></div>
	                            </div>

								<div class="form-action">
									<button type="submit" class="btn btn-form">Sektion speichern</button>
								</div>
							</form>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!--/ contact-->

	<div class="preview">
		
	</div>
@endsection