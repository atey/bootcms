@extends('layouts.backend')

@section('content')
	<!--contact-->
	<section id="contact" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="ser-title">Interneteinstellungen</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-4 col-sm-4">
			      <h3>Hinweis</h3>
			      <div class="space"></div>
			      <p>Hier kannst Du ein Logo hochladen und einen Slogan für deine Homepage definieren. <br><br> Damit deine Homepage in <b>Suchmaschienen</b> möglichst <b>weit oben</b> gelistet wird, solltest Du den <b>Webseitentitel</b> und <b>Keywords</b> entsprechend deiner Branche anpassen. <br><br> Deine Homepage ist übrigens für alle gängigen Endgeräte optimiert und sieht somit auch auf deinem Handy oder Tablet professionell aus.</p>
			    </div>
				<div class="col-md-4 col-sm-4">
			    </div>
				<div class="col-md-8 col-sm-8 marb20">
					<div class="contact-info">

						<form action="/backend/config/meta/update" method="post" role="form" class="contactForm" enctype="multipart/form-data">
                			<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<h3 class="cnt-ttl">Hintergrundeinstellungen</h3>
							<div class="space"></div>
						    <div class="form-group">
						    	<label>Webseitentitel</label>
						    	<br>
						    	<i class="fa fa-info-circle" style="font-size: 12.5px" aria-hidden="true"></i><small style="font-size: 12.5px"> Dieser Titel wird im Tab von Internetbrowsern angezeigt und <b>erscheint</b> als Webseitentitel <b>in den Suchergebnissen von Suchmaschinen</b>, wie zum Beispiel Google</small>
                                <input type="text" name="title" class="form-control br-radius-zero" value="{{ $user_info->title }}" />
                            </div>
			
						    <div class="form-group">
						    	<label>Keywords</label>
						    	<br>
						    	<i class="fa fa-info-circle" style="font-size: 12.5px" aria-hidden="true"></i><small style="font-size: 12.5px"> Suchbegriffe, unter denen deine Seite in Suchmaschinen gefunden werden kann. <b>Einzelne Keywords durch Komma trennen</b></small>
						    	<br>
						    	<i class="fa fa-info-circle" style="font-size: 12.5px" aria-hidden="true"></i><small style="font-size: 12.5px"><b> z.B. als Friseur in Köln</b> : Friseur: Friseur, Salon, Friseursalon, Köln, Haarschnitt</b></small>
                                <input type="text" name="keywords" class="form-control br-radius-zero" value="{{ $user_info->keywords }}"/>
                            </div>
                            
							<div class="form-action">
								<button type="submit" class="btn btn-form">Speichern</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/ contact-->

	<div class="preview">
		
	</div>

@endsection