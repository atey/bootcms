@extends('layouts.backend')

@section('content')
	<!--contact-->
	<section id="contact" class="section-padding">
		<form action="/backend/article/store" method="post" role="form" class="contactForm">
		<div class="container">
			<div class="row">
				@include('backend.article.components.existingTable')
				<div class="col-md-12">
					<hr>
				</div>
				<div class="col-md-12">
					<h2 class="ser-title">Sektion anlegen</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-4 col-sm-4">
			      <h3>Kurzbeschreibung</h3>
			      <div class="space"></div>
			      <p>Hier kannst Du neue Sektionen für deine Homepage anlegen. Neue Sektionen werden automatisch als Überschriften in deiner Navigationsleiste angzeigt. Wähle einfach ein Design für deine neue Sektion und fülle die Eingabefelder mit deinen Texten. Die Vorschau zeigt Dir, wie die neue Sektion auf deiner Homepage aussieht!</p>
			    </div>

        			<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="col-md-8 col-sm-8 marb20">
						<div class="contact-info">
							<h3 class="cnt-ttl">Erstelle neue Sektionen für deine Homepage</h3>
							<div class="space"></div>

							    <div class="form-group">
							    	<label>Sektion</label>
		                            <select  name="section_id" class="form-control br-radius-zero show-preview" required="required">
		                            	<option value="0" data-amountheads="-1">Design auswählen</option>
		                            	@foreach($sections as $section)
		                            	<option value="{{$section->id}}" data-amountheads="{{$section->amount_heads}}" data-title="{{$section->title}}">{{$section->description}}</option>
		                            	@endforeach
		                            </select>
	                            </div>
	                            
							    <div class="form-group">
							    	<label>Überschrift für die Navigation</label>
							    	<br>
							    	<i class="fa fa-info-circle" style="font-size: 12.5px" aria-hidden="true"></i><small style="font-size: 12.5px"> Wird bei kleineren Sektionen nicht angezeigt (z.B. "Zitat")</small>
	                                <input type="text" name="title_nav" class="form-control br-radius-zero article-enable-form" disabled="disabled" required="required" />
	                            </div>
						</div>
					</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<hr>
				<br>
				<h2 class="text-center">Vorschau</h2>
				<div id="preview-info" class="text-center hidden" style="font-size: 13px;">
					<br>
					<i class="fa fa-info-circle" aria-hidden="true"></i> Die Texte kannst Du weiter unten bearbeiten
				</div>
				<br>
				<div id="preview"></div>
				<hr>
				<h2 class="text-center">Texte bearbeiten</h2>
				<br>
			</div>
		</div>
		<div class="container">
			<div class="row">
					<div class="col-md-4 col-sm-4">
					    <div class="form-group">
					    	<label>Haupt-Überschrift und Titel für Sektion</label>
                            <input type="text" name="title" class="form-control br-radius-zero article-enable-form" disabled="disabled" required="required" />
                        </div>
                        <div class="form-group">
					    	<label>Text unter Haupt-Überschrift</label>
                            <textarea class="form-control br-radius-zero article-enable-form" name="title_text" rows="5" data-rule="required" disabled="disabled" required="required" ></textarea>
                        </div>
				    </div>
					<div class="col-md-8 col-sm-8 marb20">
                        <div class="row heads-input">
                        	<div id="heads-forms" class="col-xs-12 col-sm-12 col-md-6 col-lg-6 hidden">
							    <div class="form-group">
					    			<label>Überschrift Bereich</label>
	                                <input type="text" name="content_heads[]" class="form-control br-radius-zero" required="required"/>
	                            </div>
	                            <div class="form-group">
					    			<label>Text unter Überschrift Bereich</label>
	                                <textarea class="form-control br-radius-zero" name="content_texts[]" rows="5" data-rule="required" required="required" ></textarea>
	                            </div>
                        	</div>
                        </div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-action">
							<button type="submit" class="btn btn-form pull-right">Sektion Speichern</button>
						</div>
					</div>
			</div>
		</div>
		</form>
	</section>

@endsection