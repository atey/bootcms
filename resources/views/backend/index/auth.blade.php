@extends('layouts.backend')

@section('content')
	<section id="contact" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="ser-title">Kontaktinformationen</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-4 col-sm-4">
			      <div class="space"></div>

			      <p>Hier kannst Du deine Kontaktdaten hinterlegen! Die Kontaktdaten werden im unteren Bereich deiner Homepage angezeigt.</p>
			      <p>Die Informationen, die du hier eingibst, werden automatisch deinem Impressum hinzugefügt.</p>
			    </div>
				<div class="col-md-8 col-sm-8 marb20">
					<div class="contact-info">
							<div class="space"></div>
							<form action="/backend/auth/update" method="post" role="form" class="contactForm">
                    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			                    @if($errors->has('email'))
			                        <div class="alert alert-danger">
			                            <ul>
			                                @foreach($errors->get('email') as $error)
			                                    <li> {{ $error }}</li>
			                                @endforeach
			                            </ul>
			                        </div>
			                    @endif
			                    @if($errors->has('new_password'))
			                        <div class="alert alert-danger">
			                            <ul>
			                                @foreach($errors->get('new_password') as $error)
			                                    <li> {{ $error }}</li>
			                                @endforeach
			                            </ul>
			                        </div>
			                    @endif

							    <div class="form-group">
							    	<label>E-Mail-Adresse</label>
	                                <input type="email" name="email" class="form-control br-radius-zero" placeholder="Firmenname oder Inhaber" value="{{$user_info->email}}" required="required" />
	                            </div>
							    <div class="form-group">
							    	<label>Altes Passwort</label>
	                                <input type="password" name="password" class="form-control br-radius-zero" required="required" />
	                            </div>
							    <div class="form-group">
							    	<label>Neues Passwort</label>
	                                <input type="password" name="new_password" class="form-control br-radius-zero" />
	                            </div>
							    <div class="form-group">
							    	<label>Neues Passwort wiederholen</label>
	                                <input type="password" name="new_password_confirmation" class="form-control br-radius-zero" />
	                            </div>

								<div class="form-action">
									<button type="submit" class="btn btn-form">Einstellungen Speichern!</button>
								</div>
							</form>
					</div>
				</div>

			</div>
		</div>
	</section>

	<div class="preview"></div>

@endsection