@extends('layouts.backend')

@section('content')
	<!--contact-->
	<section id="contact" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="ser-title">Impressum und AGBs ändern</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-12 col-sm-12 marb20">
					<div class="contact-info">
							<form action="/backend/agbs/update" method="post" role="form" class="contactForm">
                    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
	                            <div class="row heads-input">
	                            	<div id="heads-forms" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
	                            		<h4>Impressum</h4>
	                            		<span style="font-size: 13px;">
	                            			<i class="fa fa-info-circle" aria-hidden="true"></i> Deine Kontaktdaten werden automatisch im Impressum angezeigt
	                            		</span>
			                            <div class="form-group">
			                                <textarea class="form-control br-radius-zero" name="impressum" rows="25" data-rule="required" placeholder="">{{ $config->impressum }}</textarea>
			                            </div>
	                            	</div>
	                            	<div id="heads-forms" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			                            <div class="form-group">
			                            	<h4>AGBs</h4>
			                            	<br>
			                                <textarea class="form-control br-radius-zero" name="agb" rows="25" data-rule="required" placeholder="">{{ $config->agb }}</textarea>
			                            </div>
	                            	</div>
	                            </div>
	                            
								<div class="form-action">
									<button type="submit" class="btn btn-form">Speichern!</button>
								</div>
							</form>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!--/ contact-->

	<div class="preview">
		
	</div>

@endsection