@extends('layouts.backend')

@section('content')
	<section id="contact" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="ser-title">Social Media Accounts verlinken</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="space"></div>
					<p>Du kannst deine Homepage ganz einfach mit deinen Social Media Accounts wie Facebook oder Twitter verlinken! Die Links werden dann automatisch angezeigt und deine Homepagebesucher können sich mit Dir vernetzen.</p>
					<p>
				    	<br>
				    	<i class="fa fa-info-circle" style="font-size: 16.5px" aria-hidden="true"></i><small style="font-size: 16.5px"> Achte darauf die ganze URL einzufügen (<b>https://</b>twitter.com/thetechcrack)</small>
					</p>
			    </div>
				<div class="col-md-8 col-sm-8 marb20">
					<div class="contact-info">
							<div class="space"></div>
							<form action="/backend/config/update" method="post" role="form" class="contactForm">
                    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
							    <div class="form-group">
							    	<label>Facebook-Link</label>
	                                <input type="text" name="facebook" class="form-control br-radius-zero" placeholder="Facebook-Link" value="{{ $user_info->facebook }}"/>
	                            </div>
							    <div class="form-group">
							    	<label>Twitter-Link</label>
	                                <input type="text" name="twitter" class="form-control br-radius-zero" placeholder="Twitter-Adresse" value="{{$user_info->twitter}}"/>
	                            </div>
							    <div class="form-group">
							    	<label>Google+-Link</label>
	                                <input type="text" name="googleplus" class="form-control br-radius-zero" placeholder="Google+-Adresse" value="{{$user_info->googleplus}}"/>
	                            </div>
	                            
							    <div class="form-group">
							    	<label>LinkedIn-Link</label>
	                                <input type="text" name="linkedin" class="form-control br-radius-zero" placeholder="LinkedIn-Adresse" value="{{$user_info->linkedin}}" />
	                            </div>
	                            
								<div class="form-action">
									<button type="submit" class="btn btn-form">Einstellungen Speichern!</button>
								</div>
							</form>
					</div>
				</div>

			</div>
		</div>
	</section>

@endsection