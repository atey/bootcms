@extends('layouts.backend')

@section('content')
	<section id="contact" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="ser-title">Kontaktinformationen</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-4 col-sm-4">
			      <div class="space"></div>

			      <p>Hier kannst Du deine Kontaktdaten hinterlegen! Die Kontaktdaten werden im unteren Bereich deiner Homepage angezeigt.</p>
			      <p>Die Informationen, die du hier eingibst, werden automatisch deinem Impressum hinzugefügt.</p>
			    </div>
				<div class="col-md-8 col-sm-8 marb20">
					<div class="contact-info">
							<div class="space"></div>
							<form action="/backend/config/update" method="post" role="form" class="contactForm">
                    			<input type="hidden" name="_token" value="{{ csrf_token() }}">

							    <div class="form-group">
							    	<label>Firmenname oder Inhaber</label>
	                                <input type="text" name="firm" class="form-control br-radius-zero" placeholder="Firmenname oder Inhaber" value="{{$user_info->firm}}" required="required" />
	                            </div>
							    <div class="form-group">
							    	<label>Adresse/Straße</label>
	                                <input type="text" name="address" class="form-control br-radius-zero" placeholder="Adresse" value="{{$user_info->address}}" required="required" />
	                            </div>
							    <div class="form-group">
							    	<label>Hausnummer</label>
	                                <input type="text" name="address_number" class="form-control br-radius-zero" placeholder="Hausnummer" value="{{$user_info->address_number}}" required="required" />
	                            </div>
							    <div class="form-group">
							    	<label>Stadt</label>
	                                <input type="text" name="city" class="form-control br-radius-zero" placeholder="Stadt" value="{{$user_info->city}}" required="required" />
	                            </div>
							    <div class="form-group">
							    	<label>Postleitzahl</label>
	                                <input type="text" name="zip_code" class="form-control br-radius-zero" placeholder="Postleitzahl" value="{{$user_info->zip_code}}" required="required" />
	                            </div>
							    <div class="form-group">
							    	<label>Telefonnummer</label>
	                                <input type="text" name="phone" class="form-control br-radius-zero" placeholder="Telefonnummer" value="{{$user_info->phone}}"/>
	                            </div>
							    <div class="form-group">
							    	<label>E-Mail-Adresse</label>
	                                <input type="text" name="email" class="form-control br-radius-zero" placeholder="E-Mail-Adresse" value="{{$user_info->email}}" required="required" />
	                            </div>

								<div class="form-action">
									<button type="submit" class="btn btn-form">Einstellungen Speichern!</button>
								</div>
							</form>
					</div>
				</div>

			</div>
		</div>
	</section>

	<div class="preview"></div>

@endsection