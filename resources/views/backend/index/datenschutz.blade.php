@extends('layouts.backend')

@section('content')
	<section id="contact" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="ser-title">Datenschutzerklärung ändern</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-12 col-sm-12 marb20">
					<div class="contact-info">
						<form action="/backend/datenschutz/update" method="post" role="form" class="contactForm">
                			<input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row heads-input">
                            	<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xs-offset-2">
		                            <div class="form-group">
		                                <textarea class="form-control br-radius-zero" name="datenschutz" rows="25" data-rule="required" placeholder="">{{ $config->datenschutz }}</textarea>
		                            </div>
                            	</div>
                            </div>
                            
							<div class="form-action">
								<button type="submit" class="btn btn-form">Speichern!</button>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>
	</section>
@endsection