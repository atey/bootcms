@extends('layouts.backend')

@section('content')
	<!--contact-->
	<section id="contact" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="ser-title">Sag Hallo Welt !</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-4 col-sm-4">
			      <h3>Kopfbereich bearbeiten</h3>
			      <div class="space"></div>
			      <p>Hier kannst Du den oberen Bereich deiner Homepage bearbeiten</p>
			    </div>
				<div class="col-md-8 col-sm-8 marb20">
					<div class="contact-info">
						<div class="space"></div>

	                    @if($errors->has('head'))
	                        <div class="alert alert-danger">
	                            <ul>
	                                @foreach($errors->get('head') as $error)
	                                    <li> {{ $error }}</li>
	                                @endforeach
	                            </ul>
	                        </div>
	                    @endif
						<form action="/backend/config/store" method="post" role="form" class="contactForm" enctype="multipart/form-data">
                			<input type="hidden" name="_token" value="{{ csrf_token() }}">

						    <div class="form-group">
						    	<label>Große Überschrift im Kopfbereich</label>
                                <input type="text" name="title_head" class="form-control br-radius-zero" placeholder="Überschrift Header" value="{{ $user_info->title_head }}" required="required" >
                            </div>
			
						    <div class="form-group">
						    	<label>Kleine Überschrift im Kopfbereich </label>
                                <input type="text" name="title_head_text" class="form-control br-radius-zero" placeholder="Text Header"   value="{{ $user_info->title_head_text }}" required="required"/>
                            </div>
						    <div class="form-group">
						    	<label>Hintergrundbild Kopfbereich</label>
						    	<img src="{{ $user_info->pic_head }}" class="img-responsive" alt="Image">
                                <input type="file" name="head" class="form-control br-radius-zero" placeholder="Bild hoch laden"/>
                            </div>
						    <div class="form-group">
						    	<label>Hintergrundfarbe statt Hintergrundbild Kopfbereich</label>
						    	<br>
						    	<i class="fa fa-info-circle" style="font-size: 12.5px" aria-hidden="true"></i><small style="font-size: 12.5px"> Die Hintergundfarbe wird mit einem Filter überzogen und damit den Webseitenfarben angepasst</small>
                                <input type="color" name="background_color_head" class="form-control br-radius-zero" value="{{ $user_info->background_color_head }}"/>
                            </div>
						    <div class="form-group">
						    	<label>Hintergrundfarbe deaktiviern</label> <br>
                                <input type="checkbox" value="disabled" name="background_color_head_disabled"> Löscht den aktuellen Wert und zeigt das Bild wieder an
                            </div>
                            
							<div class="form-action">
								<button type="submit" class="btn btn-form">Einstellungen speichern!</button>
							</div>
						</form>
						<hr>
						<form action="/backend/config/logo/update" method="post" role="form" class="contactForm" enctype="multipart/form-data">
                			<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<h3 class="cnt-ttl">Logo und Slogan</h3>
							<div class="space"></div>
		                    @if($errors->has('logo'))
		                        <div class="alert alert-danger">
		                            <ul>
		                                @foreach($errors->get('logo') as $error)
		                                    <li> {{ $error }}</li>
		                                @endforeach
		                            </ul>
		                        </div>
		                    @endif
						    <div class="form-group">
						    	<label>Logo</label>
						    	<img src="{{ $user_info->pic_logo }}" class="img-responsive" alt="Image">
						    	<br>
						    	<i class="fa fa-info-circle" style="font-size: 12.5px" aria-hidden="true"></i><small style="font-size: 12.5px"> wird meist im Kopfbereich deiner Homepage angezeigt</small>
                                <input type="file" name="logo" class="form-control br-radius-zero" placeholder="Bild hoch laden"   />
                            </div>
						    <div class="form-group">
						    	<label>Slogan</label>
						    	<br>
						    	<i class="fa fa-info-circle" style="font-size: 12.5px" aria-hidden="true"></i><small style="font-size: 12.5px"> hier kannst Du ein Zitat oder kurzen Text auf deiner Homepage einfügen</small>
                                <input type="text" name="slogan" class="form-control br-radius-zero" placeholder="Slogan" value="{{ $user_info->slogan }}" required="required" />
                            </div>
                            
							<div class="form-action">
								<button type="submit" class="btn btn-form">Speichern</button>
							</div>
						</form>
					</div>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
					<br>
					<hr>
					<div><a href="/backend/webconfig/edit">Hier kommst Du zur Bearbeitung deiner Webseiteneinstellungen</a></div>
					<hr>
				</div>

			</div>
		</div>
	</section>
	<!--/ contact-->

	<div class="preview">
		
	</div>

@endsection