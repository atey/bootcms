@extends('layouts.backend')

@section('content')
	<!--team-->
	<section id="doctor-team" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="ser-title">Bereits angelegte Mitarbeiter</h2>
					<hr class="botm-line">
				</div>
				@foreach($members as $member)
				<div class="col-md-3 col-sm-3 col-xs-6">
			      <div class="thumbnail"> 
			      	<img src="{{ $member->pic }}" alt="..." class="team-img">
			        <div class="caption">
			          <h3>{{ $member->name }}</h3>
			          <p>{{ $member->title }}</p>
			          <ul class="list-inline">
						@if(!empty($member->facebook))
			            <li><a href="{{ $member->facebook }}"><i class="fa fa-facebook"></i></a></li>
			            @endif
						@if(!empty($member->twitter))
			            <li><a href="{{ $member->twitter }}"><i class="fa fa-twitter"></i></a></li>
			            @endif
						@if(!empty($member->googleplus))
			            <li><a href="{{ $member->googleplus }}"><i class="fa fa-google-plus"></i></a></li>
			            @endif
			          </ul>
			        </div>
			      </div>
			      <div class="row">
			      	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="thumbnail"> 
							<div class="caption text-center">
								<h4><a href="/backend/member/edit/{{ $member->id }}" title="beabreiten"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></h4>
							</div>
						</div>
			      	</div>
			      	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="thumbnail"> 
							<div class="caption text-center">
								<h4><a data-toggle="modal" href='#modal-member{{ $member->id }}'><span class="fa fa-trash " title="löschen"></span></a></h4>
								<!-- Modal deleteACK START -->
								<div class="modal fade" id="modal-member{{ $member->id }}">
									<div class="modal-dialog">
										<div class="modal-content text-center">
											<div class="modal-header">
												<h4 class="modal-title">{{ $member->name }} wirklich entfernen ?</h4>

												<a href="/backend/member/destroy/{{ $member->id }}"><i class="fa fa-check pull-left btn-action" aria-hidden="true" title="endgültig löschen"></i></a>

												<span><i class="fa fa-ban close btn-action" title="nicht löschen" data-dismiss="modal" aria-hidden="true"></i></span>
											</div>
										</div>
									</div>
								</div>
								<!-- Modal deleteACK END -->
							</div>
						</div>
				      		
				      	</div>
			      </div>
			    </div>
				@endforeach

			</div>
		</div>
	</section>
	<!--/ team-->
	<!--contact-->
	<section id="contact" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="ser-title">Mitarbeiter hinzufügen</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-4 col-sm-4">
			      <h3>Hinweis</h3>
			      <div class="space"></div>
			      <p>Hier kannst Du Mitarbeiter hinzufügen und über den Bearbeiten Button oben, bereits bestehende Mitarbeiter bearbeiten!</p>
					<p>
				    	<br>
				    	<i class="fa fa-info-circle" style="font-size: 16.5px" aria-hidden="true"></i><small style="font-size: 16.5px"> Achte darauf die ganze URL einzufügen (<b>https://</b>twitter.com/thetechcrack)</small>
					</p>
			    </div>
				<div class="col-md-8 col-sm-8 marb20">
					<div class="contact-info">
							<div class="space"></div>
		                    @if($errors->has('pic'))
		                        <div class="alert alert-danger">
		                            <ul>
		                                @foreach($errors->get('pic') as $error)
		                                    <li> {{ $error }}</li>
		                                @endforeach
		                            </ul>
		                        </div>
		                    @endif

							<form action="/backend/member/store" method="post" role="form" class="contactForm" enctype="multipart/form-data">
                    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
							    <div class="form-group">
							    	<label>Anzeigename *</label>
						    	<br>
						    	<i class="fa fa-info-circle" style="font-size: 12.5px" aria-hidden="true"></i><small style="font-size: 12.5px"> z.B. Vor- und Nachname</small>
	                                <input type="text" name="name" class="form-control br-radius-zero" required="required"/>
	                            </div>
							    <div class="form-group">
							    	<label>Titel</label>
						    	<br>
						    	<i class="fa fa-info-circle" style="font-size: 12.5px" aria-hidden="true"></i><small style="font-size: 12.5px"> z.B. Doktor oder Dr.</small>
	                                <input type="text" name="title" class="form-control br-radius-zero" />
	                            </div>
				
							    <div class="form-group">
							    	<label>E-Mail-Adresse</label>
	                                <input type="text" name="member_email" class="form-control br-radius-zero" />
	                            </div>
							    <div class="form-group">
							    	<label>Facebook-Adresse</label>
	                                <input type="text" name="facebook" class="form-control br-radius-zero" />
	                            </div>
							    <div class="form-group">
							    	<label>Twitter-Adresse</label>
	                                <input type="text" name="twitter" class="form-control br-radius-zero" />
	                            </div>
							    <div class="form-group">
							    	<label>Google+-Adresse</label>
	                                <input type="text" name="googleplus" class="form-control br-radius-zero" />
	                            </div>
							    <div class="form-group">
							    	<label>LinkedIn-Adresse</label>
	                                <input type="text" name="linkedin" class="form-control br-radius-zero" />
	                            </div>
							    <div class="form-group">
							    	<label>Mitarbeiter-Bild *</label>
	                                <input type="file" name="pic" class="form-control br-radius-zero" />
	                            </div>
	                            
								<div class="form-action">
									<button type="submit" class="btn btn-form">Mitarbeiter anlegen!</button>
								</div>
							</form>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!--/ contact-->

	<div class="preview">
		
	</div>

@endsection