@extends('layouts.backend')

@section('content')
	<!--contact-->
	<section id="contact" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="ser-title">aktuelle Öffnungszeiten</h2>
					<hr class="botm-line">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Tag</th>
								<th>Zeiten</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($records as $record)
							<tr>
								<td>{{ $record->content_head }}</td>
								<td>{{ $record->content_text }}</td>
								<td>
									<a data-toggle="modal" href='#modal-edit{{ $record->id }}'><span class="fa fa-pencil btn-action"></span></a>
									<div class="modal fade" id="modal-edit{{ $record->id }}">
										<div class="modal-dialog">
											<div class="modal-content text-center">
												<div class="modal-header">

													<form action="/backend/opening/update/{{$record->id}}" method="POST" class="form-inline" role="form">
                    									<input type="hidden" name="_token" value="{{ csrf_token() }}">
														<div class="form-group">
															<label class="sr-only" for="">Tag</label>
															<input type="text" class="form-control" name="content_head" value="{{ $record->content_head }}" required="required">
														</div>
														
														<div class="form-group">
															<label class="sr-only" for="">Zeit</label>
															<input type="text" class="form-control" name="content_text" value="{{ $record->content_text }}" required="required">
														</div>
													
														<button type="submit" class="btn btn-primary">Speichern</button>
													</form>

												</div>
											</div>
										</div>
									</div>
								</td>

								<td>
									<a data-toggle="modal" href='#modal-id'><span class="fa fa-trash btn-action"></span></a>
									<div class="modal fade" id="modal-id">
										<div class="modal-dialog">
											<div class="modal-content text-center">
												<div class="modal-header">
													<h4 class="modal-title">Diesen Eintrag wirklich löschen ?</h4>

													<a href="/backend/opening/delete/{{$record->id}}"><i class="fa fa-check pull-left btn-action" aria-hidden="true"></i></a>

													<span><i class="fa fa-ban close btn-action" data-dismiss="modal" aria-hidden="true"></i></span>

												</div>
											</div>
										</div>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<hr>
				</div>
				<div class="col-md-12">
			      <div class="space"></div>
					<h2 class="ser-title">Öffnungszeiten</h2>
					<hr class="botm-line">
				</div>
				<div class="col-md-4 col-sm-4">
			      <p><br><br>Je nachdem in wievielen Zeilen Du Deine Öffnungszeiten darstellen möchtest, kannst Du auf der rechten Seite die Anzahl der Eingabefelder bestimmen.</p>
			      <br><br>
			      <p style="font-size: 13px;"><i class="fa fa-info-circle" aria-hidden="true"></i> Bevor die Öffnungszeiten auf der Homepage angezeigt werden, musst Du unter Umständen erst eine "Öffnungszeiten-<a href="/backend/article/create">Sektion</a>" <a href="/backend/article/create">erstellen</a></p>
			    </div>

				<form action="/backend/opening/store" method="post" role="form" class="contactForm">
        			<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="col-md-8 col-sm-8 marb20">
						<div class="contact-info">
							<h3 class="cnt-ttl">Lege hier Deine Öffnungszeiten an</h3>
							<div class="space"></div>


							    <div class="form-group">
							    	<label>Anzahl neue Öffnungszeiten-Felder</label>
		                            <select  name="section_id" class="form-control br-radius-zero show-preview" required="required">
		                            	<option value="1" data-amountheads="1">1</option>
		                            	<option value="2" data-amountheads="2">2</option>
		                            	<option value="3" data-amountheads="3">3</option>
		                            	<option value="4" data-amountheads="4">4</option>
		                            </select>
	                            </div>
	                            
						</div>
					</div>

					<div class="col-md-8 col-sm-8">
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							    <div class="form-group">
							    	<label>Tag oder Tage</label>
	                            </div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
	                            <div class="form-group">
							    	<label>Zeiten</label>
	                            </div>
							</div>
						</div>
				    </div>

				    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				    	
				    </div>
					<div class="col-md-8 col-sm-8 marb20">
                        <div class="row heads-input">
                        	<div id="heads-forms" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        		<div class="row">
                            		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

									    <div class="form-group">
			                                <input type="text" name="content_heads[]" class="form-control br-radius-zero" placeholder="z.B. Montag bis Mittwoch" required="required"/>
			                            </div>
	                            	</div>

	                            	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			                            <div class="form-group">
			                                <input class="form-control br-radius-zero" name="content_texts[]" data-rule="required" placeholder="z.B. 09:00 bis 18:00 Uhr" required="required"/>
			                            </div>

	                            	</div>
                        		</div>
                        	</div>
                        </div>
	                            
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-action">
							<button type="submit" class="btn btn-form pull-right">Öffnungszeiten erstellen</button>
						</div>
					</div>
				</form>

			</div>
		</div>
	</section>

@endsection