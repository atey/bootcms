@extends('layouts.backend')

@section('content')
	<!--contact-->
	<section id="contact" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="ser-title">Gästebucheinträge</h2>
					<hr class="botm-line">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Nickname</th>
								<th>Nachricht</th>
								<th>Avatar</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($guestbooks as $guestbook)
							<tr>
								<td>{{ $guestbook->nick }}</td>
								<td>{{ $guestbook->body }}</td>
								<td>
									<img src="/img/template/avatar/{{ $guestbook->avatar }}.png" alt="" class="img-responsive img-circle" width="40">
								</td>
								<td>
									<select name="status" class="form-control guestbook-status" required="required">
										@if($guestbook->status == 'new')
										<option selected="true" disabled>Neu</option>
										@endif

										<option @if($guestbook->status == 'active') selected="true" @endif data-id="{{ $guestbook->id }}" value="active">Aktiv</option>
										<option @if($guestbook->status == 'disabled') selected="true" @endif data-id="{{ $guestbook->id }}" value="disabled">Inaktiv</option>
									</select>
								</td>
								<td>
									<a data-toggle="modal" href='#modal-delete'><span class="fa fa-trash btn-action"></span></a>
									<div class="modal fade" id="modal-delete">
										<div class="modal-dialog">
											<div class="modal-content text-center">
												<div class="modal-header">
													<h4 class="modal-title">Diesen Eintrag wirklich löschen ?</h4>

													<a href="/backend/guestbook/delete/{{$guestbook->id}}"><i class="fa fa-check pull-left btn-action" title="endgültig löschen" aria-hidden="true"></i></a>

													<span><i class="fa fa-ban close btn-action" title="nicht löschen" data-dismiss="modal" aria-hidden="true"></i></span>
												</div>
											</div>
										</div>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<hr>
				</div>
			</div>
		</div>
	</section>

@endsection