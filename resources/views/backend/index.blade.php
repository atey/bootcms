@extends('layouts.backend')

@section('content')
	<!--service-->
	<section id="service" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<h2 class="ser-title">Willkommen</h2>
					<hr class="botm-line">
					<p>

						Willkommen in Deinem Homepagebaukasten! Dieser Baukasten enthält alles was Du brauchst, um ganz einfach eine eigene professionelle Homepage zu erstellen! Ganz egal wofür - mit dem Baukasten kannst Du deine Pläne in kürzester  Zeit in die Tat umsetzen! Die Bedienung des Baukastens ist kinderleicht, über + Sektionen kannst du neue Bereiche erstellen, die du dann mit deinen eigenen Inhalten füllst.    

				</div>
				<div class="col-md-8 col-sm-8">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			
							<div class="service-info">
								<div class="icon">
									<a href="/backend/article/create"><i class="fa fa-newspaper-o"></i></a>
								</div>	
								<div class="icon-info">
									<h4><a href="/backend/article/create"><i class="fa fa-plus" aria-hidden="true"></i> Inhalte erstellen und bearbeiten </a></h4>
									<p>Los gehts! Erstelle hier neue Sektionen für Deine Homepage und fülle sie mit Inhalten! Du wirst überrascht sein wie einfach es ist!</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

							<div class="service-info">
								<div class="icon">
									<a href="/backend/head/edit"><i class="fa fa-pencil-square-o"></i></a>
								</div>
								<div class="icon-info">
									<h4><a href="/backend/head/edit">Kopfbereich bearbeiten</a></h4>
									<p>Sag Hallo Welt! Hier kannst Du Hintergrundbild, Logo und Überschriften für deine Homepage festlegen</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

							<div class="service-info">
								<div class="icon">
									<a href="/backend/webconfig/edit"><i class="fa fa-globe"></i></a>
								</div>
								<div class="icon-info">
									<h4><a href="/backend/webconfig/edit">Interneteinstellungen</a></h4>
									<p>Hier kannst Du Hintergrundeinstellungen zu deiner Homepage vornehmen</p>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

							<div class="service-info">
								<div class="icon">
									<a href="/backend/auth/edit"><i class="fa fa-globe"></i></a>
								</div>
								<div class="icon-info">
									<h4><a href="/backend/auth/edit">Anmeldedaten ändern</a></h4>
									<p>Hier kannst Du die Zugriffstdaten für den Baukasten ändern</p>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

							<div class="service-info">
								<div class="icon">
									<a href="/backend/opening/edit"><i class="fa fa-clock-o"></i></a>
								</div>
								<div class="icon-info">
									<h4><a href="/backend/opening/edit">Öffnungszeiten</a></h4>
									<p>Gib deine Öffnungszeiten an damit deine Sektionen und Webseite optimal dargestellt werden</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

							<div class="service-info">
								<div class="icon">
									<a href="/backend/contact/edit"><i class="fa fa-address-card-o"></i></a>
								</div>
								<div class="icon-info">
									<h4><a href="/backend/contact/edit">Kontaktinformationen</a></h4>
									<p>Hier kannst Du deine Kontaktinformationen hinterlegen und aktualisieren</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

							<div class="service-info">
								<div class="icon">
									<a href="/backend/agbs"><i class="fa fa-gavel"></i></a>
								</div>
								<div class="icon-info">
									<h4><a href="/backend/agbs">Impressum & AGBs</a></h4>
									<p>Das Impressum & die AGBs dürfen auf keiner professionellen Homepage fehlen</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">

						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<div class="service-info">
								<div class="icon">
									<a href="/backend/socialmedia/edit"><i class="fa fa-facebook-square"></i></a>
								</div>
								<div class="icon-info">
									<h4><a href="/backend/socialmedia/edit">Social Media</a></h4>
									<p>Verlinke deine Homepage mit deinen Social Media Accounts in diesem Bereich!</p>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

							<div class="service-info">
								<div class="icon">
									<a href="/backend/member/create"><i class="fa fa-users"></i></a>
								</div>
								<div class="icon-info">
									<h4><a href="/backend/member/create">Mitarbeiter anlegen und bearbeiten</a></h4>
									<p>Stelle Dich und dein Team persönlich vor!</p>
								</div>
							</div>
						</div>

					</div>
					<div class="row">

						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<div class="service-info">
								<div class="icon">
									<a href="/backend/socialmedia/edit"><i class="fa fa-user-secret"></i></a>
								</div>
								<div class="icon-info">
									<h4><a href="/backend/datenschutz">Datenschutz</a></h4>
									<p>Trage hier deine Datenschutzbestimmung ein!</p>
								</div>
							</div>
						</div>
					
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

							<div class="service-info">
								<div class="icon">
									<a href="/backend/guestbook/edit"><i class="fa fa-book"></i></a>
								</div>
								<div class="icon-info">
									<h4><a href="/backend/guestbook/edit">Gästebuch
										@if($newguestbook > 0)
										<sup>{{ $newguestbook }} neue</sup>
										@endif</a></h4>
									<p>Stelle ein, welche Nachrichten auf deiner Homepage angezeigt werden und welche nicht</p>
								</div>
							</div>
						</div>

					</div>

				</div>

				<div class="col-md-12 col-sm-12">
					@if(Auth::user()->type == 3)
					Superadminbereich
					<hr>
					<div class="service-info">
						<div class="icon">
							<a href="/backend/section/create"><i class="fa fa-cogs"></i></a>
						</div>
						<div class="icon-info">
							<h4><a href="/backend/section/create">Sektion erstellen</a></h4>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</section>
	<!--/ service-->
@endsection