@extends('layouts.master')

@section('head')
        <li><a href="/#banner">Home</a></li>
        @if($articles->count() > 0)
            @foreach($articles as $key => $article)
                @if($article->section->title_nav_active == 1)
                <li><a href="/#{{ $article->title_nav }}{{ $key }}">{{ $article->title_nav }}</a></li>
                @else
                @endif
            @endforeach
        @else
            <li><a href="/backend">Seite bearbeiten!</a></li>
        @endif
        <li class=""><a href="/#contact">Kontakt</a></li>
        @if(Auth::user())
        <li><a href="/backend"><i class="fa fa-wrench" aria-hidden="true"></i> Baukasten</a></li>
        @endif

@endsection

@section('content')
 
            @if($articles->count() > 0)
                @foreach($articles as $articlekey => $article)
                	@php
                	$title = $article->section->title;
                	@endphp
                    @include("layouts.sections.$title")
                @endforeach
            @else
                <br>
                <div class="text-center">
                    <a href="/backend"><h2 class="btn-edit">Jetzt deine Homepage bearbeiten!</h2></a>
                </div>
            @endif

            @if($members->count() > 0)
                @include("layouts.sections.team")
            @endif

            @include("layouts.sections.guestbook")

            @if($guestbooks->count() > 0)
                    @include("layouts.sections.assessments")
            @endif

	
@endsection

