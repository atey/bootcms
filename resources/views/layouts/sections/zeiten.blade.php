<section id="{{ $article->title_nav }}{{ $articlekey }}" class="section-padding">
    @include('layouts.sections.articleEdit')
	<div class="container">
		<div class="row">
			<div class="schedule-tab">
			<div class="col-md-4 col-sm-4 bor-left">
				<div class="mt-boxy-color"></div>
    			<div class="medi-info">
	    			<h3>{{ $article->title }}</h3>
					<p>{!! nl2br(e($article->title_text)) !!}</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="medi-info">
	    			<h3>{{ $article->records[0]->content_head }}</h3>
					<p>{!! nl2br(e($article->records[0]->content_text)) !!}</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 mt-boxy-3 text-center">
				<div class="mt-boxy-color"></div>
				<div class="time-info text-center">
						
		    			<h3>Öffnungszeiten @if(Auth::user())<a href="/backend/opening/edit"><i class="fa fa-pencil-square" style="color: white"></i></a>@endif</h3>
						
					    
		    			<table class="table table-responsive">
		    				<tbody>
									@if($openings->count() == 0)
										Leider wurden noch keine Öffnungzeiten angegeben!
									@else
									@foreach($openings as $opening)
		    						<tr>
										<td>{{ $opening->content_head }}</td>
										<td>{{ $opening->content_text }}</td>
		    						</tr>
									@endforeach
									@endif
		    				</tbody>
		    			</table>

				</div>
			</div>
			</div>
		</div>
	</div>
</section>
<!--cta-->