<section id="{{ $article->title_nav }}{{ $articlekey }}" class="cta-2 section-padding">
    @include('layouts.sections.articleEdit')
	<div class="container">
		<div class=" row">
			<div class="col-md-2"></div>
            <div class="text-right-md col-md-4 col-sm-4">
              <h2 class="section-title white lg-line">« {{ $article->title }} »</h2>
            </div>
            <div class="col-md-4 col-sm-5">
              {!! nl2br(e($article->title_text)) !!}
              <p class="text-right text-primary"><i>— {{ $info->firm }}</i></p>
            </div>
            <div class="col-md-2"></div>
        </div>
	</div>
</section>
