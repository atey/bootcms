<section id="contact" class="section-padding">
    @if(Auth::user())
	<div class="service-info pull-left">
		<div class="icon">
			<a href="/backend/config/edit" class="btn-edit"><i class="fa fa-newspaper-o btn-edit"></i></a>
		</div>	
	</div>
    @endif
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="ser-title">Kontakt</h2>
				<hr class="botm-line">
			</div>
			<div class="col-md-4 col-sm-4">
		      <h3>Unsere Kontakt Informationen</h3>
		      <div class="space"></div>
		      <p><i class="fa fa-map-marker fa-fw pull-left fa-2x"></i>{{$info->address}} {{$info->address_number}}<br>
		        {{$info->city}}, {{$info->zip_code}}</p>
		      <div class="space"></div>
		      <p><i class="fa fa-envelope-o fa-fw pull-left fa-2x"></i>{{$info->email}}</p>
		      <div class="space"></div>
		      <p><i class="fa fa-phone fa-fw pull-left fa-2x"></i>{{$info->phone}}</p>
		    </div>
			<div class="col-md-8 col-sm-8 marb20">
				<div class="contact-info">
						<h3 class="cnt-ttl">Schreibe einen Gästebucheintrag und lass eine Nachricht da</h3>
						<div class="space"></div>
						<form action="/send-message" method="POST" role="form" class="contactForm">
        					<input type="hidden" name="_token" value="{{ csrf_token() }}">
<!--    						    
						    <div class="form-group">
                                <input type="text" name="name" class="form-control br-radius-zero" id="nickname" placeholder="Dein Name"/>
                            </div>
                         
                            <div class="form-group">
                                <input type="email" class="form-control br-radius-zero" name="email" id="email" placeholder="Deine E-Mail-Adresse"/>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control br-radius-zero" name="subject" id="subject" placeholder="Betreff"/>
                            </div>
 -->
                            <div class="form-group">
                                <textarea class="form-control br-radius-zero" name="message" rows="5" data-rule="required" data-msg="Bitte schreibe uns etwas!" placeholder="Nachricht!"></textarea>
                            </div>
                            
							<div class="form-action">
								<button type="submit" class="btn btn-form">Eintrag abschicken!</button>
							</div>
						</form>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/ contact-->