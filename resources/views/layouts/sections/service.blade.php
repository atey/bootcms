<section id="{{ $article->title_nav }}{{ $articlekey }}" class="services section-padding">
    @include('layouts.sections.articleEdit')
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<h2 class="ser-title">{{ $article->title }}</h2>
				<hr class="botm-line">
				<p>{!! nl2br(e($article->title_text)) !!}</p>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="service-info">
					<div class="icon-info">
						<h4>{{ $article->records[0]->content_head }}</h4>
						<p>{!! nl2br(e($article->records[0]->content_text)) !!}</p>
					</div>
				</div>
				<div class="service-info">
					<div class="icon-info">
						<h4>{{ $article->records[2]->content_head }}</h4>
						<p>{!! nl2br(e($article->records[2]->content_text)) !!}</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="service-info">
					<div class="icon-info">
						<h4>{{ $article->records[1]->content_head }}</h4>
						<p>{!! nl2br(e($article->records[1]->content_text)) !!}</p>
					</div>
				</div>
				<div class="service-info">
					<div class="icon-info">
						<h4>{{ $article->records[3]->content_head }}</h4>
						<p>{!! nl2br(e($article->records[3]->content_text)) !!}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
