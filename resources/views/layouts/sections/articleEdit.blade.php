@if(Auth::user())
	@if(Request::is('backend/article/edit/*'))
	@else
	<div class="service-info pull-left">
	  <div class="icon">
	    @if($article->position - 1 != 0)
	    <i type="button" class="fa fa-arrow-up btn-edit change-position" data-id="{{ $article->id}}" data-new-position="{{ $article->position - 1 }}" aria-hidden="true"></i><br>
	    @endif
	      <a href="/backend/article/edit/{{$article->id}}" class="btn-edit"><i class="fa fa-pencil-square btn-edit"></i></a><br>
	    @if($articles->count() != $article->position)
	    <i type="button" class="fa fa-arrow-down btn-edit change-position" data-id="{{ $article->id}}" data-new-position="{{ $article->position + 1 }}" aria-hidden="true"></i><br>
	    @endif
	  </div>  
	</div>
	@endif
@endif