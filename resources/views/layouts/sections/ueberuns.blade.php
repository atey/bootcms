<section id="{{ $article->title_nav }}{{ $articlekey }}" class="about section-padding">
    @include('layouts.sections.articleEdit')
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-4 col-xs-12">
		        <div class="section-title">
		          <h2 class="head-title lg-line">{{$article->title}}</h2>
		          <hr class="botm-line">
		          <p class="sec-para">{!! nl2br(e($article->title_text)) !!}</p>
		        </div>
		    </div>
		    <div class="col-md-9 col-sm-8 col-xs-12">
		       <div style="visibility: visible;" class="col-sm-9 more-features-box">
		          <div class="more-features-box-text">
		            <div class="more-features-box-text-icon"> <i class="fa fa-angle-right" aria-hidden="true"></i> </div>
		            <div class="more-features-box-text-description">
			            <h3>{{ $article->records[0]->content_head }}</h3>
			            <p>{!! nl2br(e($article->records[0]->content_text)) !!}</p>
			        </div>
		          </div>
		          <div class="more-features-box-text">
		            <div class="more-features-box-text-icon"> <i class="fa fa-angle-right" aria-hidden="true"></i> </div>
		            <div class="more-features-box-text-description">
			            <h3>{{ $article->records[1]->content_head }}</h3>
			            <p>{!! nl2br(e($article->records[1]->content_text)) !!}</p>
			        </div>
                    </div>
		        </div>
		    </div>
		</div>
	</div>
</section>
