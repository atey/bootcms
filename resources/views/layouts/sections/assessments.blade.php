<section id="assessments" class="section-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="ser-title">Gästebuch</h2>
				<hr class="botm-line">
			</div>
			@foreach($guestbooks as $guestbook)
			<div class="col-md-4 col-sm-4">
				<div class="testi-details">
					<!-- Paragraph -->
					<p> {{ $guestbook->body }} </p>
				</div>
				<div class="testi-info">
					<!-- User Image -->
					<img src="img/template/avatar/{{ $guestbook->avatar }}.png" alt="" class="img-responsive img-circle" width="75">
					<!-- User Name -->
					<h3> {{ $guestbook->nick }} </h3>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>
