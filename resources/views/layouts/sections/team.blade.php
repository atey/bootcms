<section id="doctor-team" class="section-padding">
    @if(Auth::user())
	<div class="service-info pull-left">
		<div class="icon">
			<a href="/backend/member/create" class="btn-edit"><i class="fa fa-users btn-edit"></i></a>
		</div>	
	</div>
    @endif
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="ser-title">Treffe die Mitarbeiter!</h2>
				<hr class="botm-line">
			</div>
			@foreach($members as $member)
			<div class="col-md-3 col-sm-3 col-xs-6">
		      <div class="thumbnail"> 
		      	<img src="{{ $member->pic }}" alt="..." class="team-img">
		        <div class="caption">
		          <h3>{{ $member->name }}</h3>
		          <p>{{ $member->title }}</p>
		          <p>{{ $member->member_email }}</p>
		          <ul class="list-inline">
					@if(!empty($member->facebook))
		            <li><a href="{{ $member->facebook }}"><i class="fa fa-facebook"></i></a></li>
		            @endif
					@if(!empty($member->twitter))
		            <li><a href="{{ $member->twitter }}"><i class="fa fa-twitter"></i></a></li>
		            @endif
					@if(!empty($member->googleplus))
		            <li><a href="{{ $member->googleplus }}"><i class="fa fa-google-plus"></i></a></li>
		            @endif
		          </ul>
		        </div>
		      </div>
		    </div>
			@endforeach

		</div>
	</div>
</section>
