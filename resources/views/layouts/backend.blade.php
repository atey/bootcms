<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>noobCMS</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
    
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
    <link rel="stylesheet" href="/css/all.css">
        <!-- =======================================================
        Theme Name: Medilab & noobCMS
        Theme URL: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/
        Author: Artur Smolen
        Author URL: smoart.de
    ======================================================= -->
  </head>
  <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
    <div id="app">
	  	<!--banner-->
		<section id="bannerbackend" class="banner">
			<div class="bg-colorbackend">
				<nav class="navbar navbar-default navbar-fixed-top">
				  <div class="container">
				  	<div class="col-md-12">
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
						      </button>
					      <a class="navbar-brand" href="/"><img src="/img/cms/logo.png" class="img-responsive" style="width: 140px; margin-top: -16px;"></a>
					    </div>
					    <div class="collapse navbar-collapse navbar-right" id="myNavbar">
					      <ul class="nav navbar-nav">
					        <li {{{ (Request::is('admin') ? 'class=active' : '') }}}><a href="/admin"><i class="fa fa-wrench" aria-hidden="true"></i> Baukasten</a></li>
					        <li {{{ (Request::is('backend/article/create') ? 'class=active' : '') }}}><a href="/backend/article/create"><i class="fa fa-plus" aria-hidden="true"></i> Inhalte</a></li>
					        <li><a href="/"><i class="fa fa-desktop" aria-hidden="true"></i> Homepage</a></li>
					        <li {{{ (Request::is('backend/faqs') ? 'class=active' : '') }}}><a href="/backend/faqs"><i class="fa fa-info-circle" aria-hidden="true"></i> Starthilfe</a></li>

	                        @guest
	                            <li><a href="{{ route('login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a></li>
	                        @else
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i> Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
	                        @endguest
					      </ul>
					    </div>
					</div>
				  </div>
				</nav>
				<div class="container">
					<div class="row">
						<div class="banner-infobackend">
							<div class="banner-logo text-center">
								<img src="/img/cms/logo.png" class="img-responsive">
							</div>
							<div class="banner-text text-center">
								<h1 class="white">Dein Homepagebaukasten!</h1>
								<p>Hier findest Du alle Werkzeuge und Einstellungen um ganz einfach Deine eigene Homepage zu bauen.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ banner-->
		
		@yield('content')

		<!--footer-->
		<footer id="footer">
			<div class="top-footer">
				<div class="container">
					<div class="row">
					</div>
				</div>
			</div>
			<div class="footer-line">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							© Copyright Medilab Theme. All Rights Reserved
                            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--/ footer-->
	</div>
    <script src="{{ mix('/js/all.js') }}"> </script>
    <script src="{{ mix('/js/app.js') }}"> </script>

    
  </body>
</html>