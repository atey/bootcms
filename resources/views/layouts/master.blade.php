<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $info->config->title }}</title>
    <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
    <meta name="keywords" content="{{ $info->config->keywords }}">
    
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
        <!-- =======================================================
        Theme Name: Medilab
        Theme URL: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/
        Author: Artur Smolen
        Author URL: smoart.de
    ======================================================= -->
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#eaf7f7",
	      "text": "#5c7291"
	    },
	    "button": {
	      "background": "#56cbdb",
	      "text": "#ffffff"
	    }
	  },
	  "theme": "edgeless",
	  "content": {
	    "message": "Diese Webseite nutzt Cookies um die Benutzerfreundlichkeit zu erhöhen.",
	    "dismiss": "Verstanden",
	    "link": "Mehr"
	  }
	})});
	</script>
  </head>
  <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
    <div id="app">
	  	<!--banner-->

		<section id="banner" class="banner" style="
	  	@if(!empty($info->config->background_color_head))
	  	background-color: {{ $info->config->background_color_head }}
	  	@else
	  	background: url('{{ $info->config->pic_head }}') no-repeat fixed
	  	@endif ">
		    @if(Auth::user())
			<div class="service-info pull-left">
				<br><br><br><br><br><br>
				<div class="icon">
					<a href="/backend/head/edit" class="btn-edit"><i class="fa fa-pencil-square btn-edit"></i></a>
				</div>	
			</div>
		    @endif
			<div class="bg-color">
				<nav class="navbar navbar-default navbar-fixed-top">
				  <div class="container">
				  	<div class="col-md-12">
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					      <a class="navbar-brand" href="/"><img src="{{ $info->config->pic_logo }}" class="img-responsive" style="width: 140px; margin-top: -16px;"></a>
					    </div>
					    <div class="collapse navbar-collapse navbar-right" id="myNavbar">
					      <ul class="nav navbar-nav">
					      	@yield('head')
					      </ul>
					    </div>
					</div>
				  </div>
				</nav>
				<div class="container">
					<div class="row">
						<div class="banner-info">
							<div class="banner-logo text-center">
								<img src="{{ $info->config->pic_logo }}" class="img-responsive">
							</div>
							<div class="banner-text text-center">
								<h1 class="white">{{ $info->config->title_head }}</h1>
								<p>{{ $info->config->title_head_text }}</p>
								<!-- <a href="#contact" class="btn btn-appoint">Make an Appointment.</a> -->
							</div>
							<div class="overlay-detail text-center">
					            @if($articles->count() > 0)
				               		<a href="/#{{ $articles->first()->title_nav }}0"><i class="fa fa-angle-down"></i></a>
				               	@else
				               		<a href="/#contact"><i class="fa fa-angle-down"></i></a>
					            @endif
				             </div>		
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ banner-->
		
		@yield('content')
		<!--footer-->
		<footer id="footer">
			<div class="top-footer">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-4 marb20">
								<div class="ftr-tle">
									<h4 class="white no-padding">Über uns</h4>
								</div>
								<div class="info-sec">
									<p>{{ $info->config->slogan }}</p>
								</div>
						</div>
						<div class="col-md-4 col-sm-4 marb20">
							<div class="ftr-tle">
								<h4 class="white no-padding">Rechtliches</h4>
							</div>
							<div class="info-sec">
								<ul class="quick-info">
									<li><a href="/"><i class="fa fa-circle"></i>Home</a></li>
									<li><a href="/impressum"><i class="fa fa-circle"></i>Impressum</a></li>
									@if(!empty($info->agb))
										<li><a href="/agbs"><i class="fa fa-circle"></i>AGBs</a></li>
									@endif
									@if(!empty($info->datenschutz))
										<li><a href="/datenschutz"><i class="fa fa-circle"></i>Datenschutz</a></li>
									@endif
								</ul>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 marb20">
							<div class="ftr-tle">
								<h4 class="white no-padding">Folge uns</h4>
							</div>
							<div class="info-sec">
								<ul class="social-icon">
									@if(!empty($info->facebook))
									<a href="{{ $info->facebook }}"><li class="bglight-blue"><i class="fa fa-facebook"></i></li></a>
									@endif
									@if(!empty($info->googleplus))
									<a href="{{ $info->googleplus }}"><li class="bgred"><i class="fa fa-google-plus"></i></li></a>
									@endif
									@if(!empty($info->linkedin))
									<a href="{{ $info->linkedin }}"><li class="bgdark-blue"><i class="fa fa-linkedin"></i></li></a>
									@endif
									@if(!empty($info->twitter))
									<a href="{{ $info->twitter }}"><li class="bglight-blue"><i class="fa fa-twitter"></i></li></a>
									@endif
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-line">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							© Copyright Medilab Theme. All Rights Reserved
	                        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--/ footer-->
	</div>

    <script src="{{ mix('/js/app.js') }}"> </script>
    <script src="{{ mix('/js/all.js') }}"> </script>

    
  </body>
</html>