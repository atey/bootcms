<?php

use App\Models\Configuration;
use App\Models\ConfigurationUser;
use App\Models\Section;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $atzen           = new User;
        $atzen->id       = 1;
        $atzen->name     = 'atzen';
        $atzen->email    = 'smolen.art@gmail.com';
        $atzen->password = '$2y$10$nb4pisxMhSzR6Ix/rQUGeOhwM52/J7Yi.kOTGMuWV.dEfHRqoWRUy';
        $atzen->type     = 3;
        $atzen->save();

        $test           = new User;
        $test->id       = 2;
        $test->name     = 'test';
        $test->email    = 'test@cms.de';
        $test->password = bcrypt('secret');
        $test->type     = 2;
        $test->save();

        $user_info                   = new Configuration;
        $user_info->user_id          = 2;
        $user_info->firm             = 'Praxis für Logopädie & Integrative Lerntherapie';
        $user_info->address          = 'Markt';
        $user_info->address_number   = '13';
        $user_info->city             = 'Erftstadt- Lechenich';
        $user_info->zip_code         = '50374';
        $user_info->phone            = '02235 / 770605';
        $user_info->email            = 'Logopädie-dominick@web.de';
        $user_info->status           = 1;
        $user_info->save();

        $config = ConfigurationUser::create([
            'user_id'          => 2,
            'title'            => 'Logopädie Erftstadt',
            'title_head'       => 'Sylke Dominick',
            'title_head_text'  => 'Sprach- / Sprech-/ Stimmtherapien',
            'slogan'           => 'Hilfe bei Lese-Rechtschreibschwäche und Dyskalkulie',
            'pic_logo'         => 'main/logo_12356.jppg',
            'configuration_id' => $user_info->id,
        ]);

        Section::create([
            'title'             => 'service',
            'description'       => '1. Hauptsektion mit 5 Textblöcken',
            'amount_heads'      => 4,
            'amount_links'      => 1,
            'title_text_active' => 1,
            'title_nav_active'  => 1,
        ]);
        Section::create([
            'title'             => 'zeiten',
            'description'       => '2. Öffnungszeiten mit 2 Textblöcken',
            'amount_heads'      => 1,
            'amount_links'      => 1,
            'title_text_active' => 1,
            'title_nav_active'  => 0,
        ]);
        Section::create([
            'title'             => 'ueberuns',
            'description'       => '3. 3 kurze Texte mit Überschrift',
            'amount_heads'      => 2,
            'amount_links'      => 1,
            'title_text_active' => 1,
            'title_nav_active'  => 1,
        ]);
        Section::create([
            'title'             => 'zitat',
            'description'       => '4. Balken mit Zitat und einem Textblock',
            'amount_heads'      => 0,
            'amount_links'      => 1,
            'title_text_active' => 1,
            'title_nav_active'  => 0,
        ]);
    }
}
