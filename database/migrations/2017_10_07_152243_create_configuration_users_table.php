<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuration_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('configuration_id')->default('0');
            $table->string('title');
            $table->string('title_head');
            $table->string('title_head_text');
            $table->string('slogan')->default('0');
            $table->string('keywords')->nullable();
            $table->string('pic_logo')->default('0');
            $table->string('pic_head')->default('0');
            $table->string('background_color_head')->nullable();
            $table->string('background-color2')->default('0');
            $table->string('font-family')->default('0');
            $table->string('font-family2')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuration_users');
    }
}
