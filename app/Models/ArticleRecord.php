<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleRecord extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id',
        'content_head',
        'content_text',
        'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

	public function article()
	{
		return $this->belongsTo(Article::class);
	}
}
