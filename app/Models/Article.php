<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'section_id',
        'title_nav',
        'title',
        'title_text',
        'position',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
    ];

	public function section()
	{
		return $this->belongsTo(Section::class);
	}
	public function records()
	{
		return $this->hasMany(ArticleRecord::class);
	}
}
