<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firm',
        'address',
        'address_number',
        'city',
        'zip_code',
        'phone',
        'email',
        'impressum',
        'agb',
        'datenschutz',
        'facebook',
        'twitter',
        'googleplus',
        'likedin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
    ];
    
    public function config()
    {
        return $this->hasOne(ConfigurationUser::class, 'configuration_id');
    }
}
