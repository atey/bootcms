<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigurationUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'title_head',
        'title_head_text',
        'slogan',
        'keywords',
        'pic_logo',
        'pic_head',
        'background_color_head',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'status',
    ];

    public function config()
    {
        return $this->belongsTo(Configuration::class, 'configuration_id');
    }

}
