<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleRecord;
use App\Models\Configuration;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $sections = Section::all();
        $articles = Article::all()->sortBy('position');
        return view('backend.index.article', compact('sections', 'articles'));

    }

    public function store(Request $request)
    {
        $position = Article::all()->count() + 1;
        $title_nav = str_replace(' ', '', $request->title_nav);
        $article = Article::create([
                'user_id'    => Auth::id(),
                'section_id' => $request->section_id,
                'title'      => $request->title,
                'title_text' => $request->title_text,
                'title_nav'  => $title_nav,
                'position'   => $position,

            ]);

        $section = Section::find($request->section_id);
        
        if ($section->amount_heads > 0) {
            foreach ($request->content_heads as $key => $none) {

                ArticleRecord::create([
                    'article_id'   => $article->id,
                    'content_head' => $request->content_heads[$key],
                    'content_text' => $request->content_texts[$key],
                ]);
            }
        }

        return redirect()->route('ArticleEdit', ['id' => $article->id]);
    }
    
    public function storeOpening(Request $request)
    {

        foreach ($request->content_heads as $key => $none) {

            ArticleRecord::create([
                'article_id'   => 0,
                'content_head' => $request->content_heads[$key],
                'content_text' => $request->content_texts[$key],
                'type'         => "opening",
            ]);
        }

        return back();
    }


    public function show(Article $article)
    {
        //
    }

    public function edit(Article $article, $id)
    {   
        if (Auth::user()->type < 2 || empty(Article::find($id))) {
            return back();
        }
        $article = $article->find($id);
        $articles = Article::all()->sortBy('position');
        $sections = Section::all();
        $info = Configuration::find(1);
        $openings = ArticleRecord::where('type', "opening")->get(array('content_text', 'content_head'));
        
        return view('backend.article.edit', compact('article', 'articles', 'sections', 'info', 'openings'));
    }

    public function update(Request $request, Article $article, $id)
    {
        $title_nav = str_replace(' ', '', $request->title_nav);
        Article::whereId($id)->update([
                'title'      => $request->title,
                'title_nav'  => $title_nav,
                'title_text' => $request->title_text,
            ]);
        foreach ($request->content_ids as $key => $content_id) {

            ArticleRecord::whereId($content_id)->update([
                'content_head' => $request->content_heads[$key],
                'content_text' => $request->content_texts[$key],

            ]);
        }
        return back();
    }

    public function changePosition($id, $position)
    {

        $article = Article::find($id);
        $changer = Article::where('position', $position)->first();

        $changer->position = $article->position;
        $article->position = $position;

        if ($article->save() && $changer->save()) {
            return 'true';
        }else{
            return 'false';
        }
    }

    public function destroy(Article $article)
    {
        foreach ($article->records as $key => $record) {
            $record->delete();
        }
        $article->delete();
        
        $articles = Article::all()->sortBy('position');
        foreach ($articles as $key => $article_new) {
            $article_new->update(['position' => $key + 1]);
        }
        
        return redirect()->route('Article');
    }

    public function destroyOpening(ArticleRecord $articleRecord)
    {
        $articleRecord->delete();
        
        return redirect()->route('Opening');
    }
    public function updateOpening(ArticleRecord $articleRecord, Request $request)
    {
        $articleRecord->update($request->all());
        
        return back();
    }
}
