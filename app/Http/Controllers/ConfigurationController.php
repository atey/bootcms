<?php

namespace App\Http\Controllers;

use App\Models\ArticleRecord;
use App\Models\Configuration;
use App\Models\ConfigurationUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Session;
use Validator;

class ConfigurationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function opening(Configuration $configuration)
    {
        $user_info = User::where('type', 2)->first()->config->last();
        $records = ArticleRecord::where('type', "opening")->get();

        return view('backend.index.opening', compact('user_info','records'));
    }
    public function webconfig(Configuration $configuration)
    {
        $user_info = User::where('type', 2)->first()->config->last();

        return view('backend.index.webconfig', compact('user_info'));
    }
    public function socialmedia(Configuration $configuration)
    {
        $user_info = User::where('type', 2)->first()->config->last()->config;

        return view('backend.index.socialmedia', compact('user_info'));
    }
    public function head(Configuration $configuration)
    {
        $user_info = User::where('type', 2)->first()->config->last();

        return view('backend.index.head', compact('user_info'));
    }
    public function contact(Configuration $configuration)
    {
        $user_info = User::where('type', 2)->first()->config->last()->config;
        return view('backend.index.contact', compact('user_info'));
    }

    public function auth(Configuration $configuration)
    {
        $user_info = Auth::user();
        return view('backend.index.auth', compact('user_info'));
    }



    public function update(Request $request, Configuration $configuration)
    {
        $user_info = Configuration::where('status', 1)->first();

        if (empty($user_info)) {
        $user_info = new Configuration;
        $user_info->user_id = Auth::id();
        }

        $user_info->update($request->all());


        return back();
    }

    public function updateAuth(Request $request)
    {
        
        $rules = array(
            'email' => 'email|required',
            'password' => 'required',
            'new_password' => 'required|confirmed',
            );
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }else{

            $user = Auth::user();
            if (Hash::check($request->password, $user->password)) {
                $user->email = $request->email;
                $user->save();
                if (!empty($request->new_password) && $request->new_password == $request->new_password_confirmation) {
                    $user->password = Hash::make($request->new_password);
                    $user->save();
                }
            }
        }

        return back();
    }

    public function updateMeta(Request $request)
    {
        $user_info = User::where('type', 2)->first()->config->last();
        $user_info->update([
            'title'           => $request->title,
            'keywords'        => $request->keywords,
        ]);
        return back();
    }
    public function updateHead(Request $request)
    {

        $destinationPath = 'img/main'; // upload path
        $fileName = NULL;


        $file = array('head' => $request->file('head'));
        $rules = array(
            'head' => 'mimes:jpg,jpeg,png|max:4500',
            );
        $validator = Validator::make($file, $rules);
        
        if (!empty($request->file('head')) && $validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }else{

            $user_info = User::where('type', 2)->first()->config->last();

            if (!empty($request->file('head'))) {
                if (file_exists(public_path($user_info->pic_head)) && !empty($user_info->pic_head)) {
                    unlink(public_path($user_info->pic_head));
                }
                $extension = $request->file('head')->getClientOriginalExtension(); // getting image extension
                $fileNameHead = 'head_'.rand(11111,99999).'.'.$extension; // renameing image
                $request->file('head')->move($destinationPath, $fileNameHead); // uploading file to given path
                $user_info->update([
                    'pic_head'            => '/'. $destinationPath .'/'. $fileNameHead,
                ]);
            }

            if ($request->background_color_head_disabled == "disabled") {
                $request->background_color_head = "";
            }

            $user_info->update([
                "title_head" => $request->title_head,
                "title_head_text" => $request->title_head_text,
                "background_color_head" => $request->background_color_head,

            ]);
            
        }
        return back();

    }
    public function updateLogo(Request $request)
    {

        $destinationPath = 'img/main'; // upload path
        $fileName = NULL;

        $file = array('logo' => $request->file('logo'), 'head' => $request->file('head'));
        $rules = array(
            'logo' => 'mimes:jpg,jpeg,png|max:4500',
            );
        $validator = Validator::make($file, $rules);
        
        if (!empty($request->file('logo'))&& $validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }else{

            $user_info = User::where('type', 2)->first()->config->last();

            $user_info->update([
                'slogan'            => $request->slogan,
            ]);
            if (!empty($request->file('logo'))) {
                if (file_exists(public_path($user_info->pic_logo)) && !empty($user_info->pic_logo)) {
                    unlink(public_path($user_info->pic_logo));
                }
                $extension = $request->file('logo')->getClientOriginalExtension(); // getting image extension
                $fileName = 'logo_'.rand(11111,99999).'.'.$extension; // renameing image
                $request->file('logo')->move($destinationPath, $fileName); // uploading file to given path
                $user_info->update([
                    'pic_logo'            => '/'. $destinationPath .'/'. $fileName,
                ]);
            }

            return back();
        }

    }
    public function updateAgbs(Request $request)
    {
        $user_info = Configuration::where('status', 1)->first();

        $user_info->update([
            "impressum" => $request->impressum, 
            "agb"       => $request->agb,
        ]);

        return back();
    }
    public function updateDatenschutz(Request $request)
    {
        $user_info = Configuration::where('status', 1)->first();

        $user_info->update([
            "datenschutz" => $request->datenschutz, 
        ]);

        return back();
    }
}
