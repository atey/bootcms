<?php

namespace App\Http\Controllers;

use App\Models\Guestbook;
use App\Models\Article;
use App\Models\ArticleRecord;
use App\Models\Configuration;
use App\User;
use Illuminate\Http\Request;
use View;

class HomeController extends Controller
{    


    public function __construct() {
        
        $articles = Article::all()->sortBy('position');
        $info = User::where('type', 2)->first()->config->last()->config;
        $members = User::where('type', 1)->get();
        $openings = ArticleRecord::where('type', "opening")->get(array('content_text', 'content_head'));
        $guestbooks = Guestbook::where('status', 'active')->get();

        View::share('articles', $articles );
        View::share('info', $info );
        View::share('members', $members );
        View::share('openings', $openings );
        View::share('guestbooks', $guestbooks );
    }  
   

    public function index()
    {
        return view('home');
    }

    public function home()
    {
        return view('index');
    }
    public function impressum()
    {
        return view('impressum');
    }
    public function datenschutz()
    {
        return view('datenschutz');
    }
    public function agbs()
    {
        return view('agbs');
    }

    public function mail(Request $request)
    {
        $info = Configuration::find(1);
        $empfaenger  = $info->email;
        $betreff = $request->subject;

        $nachricht = $request->message;

        // für HTML-E-Mails muss der 'Content-type'-Header gesetzt werden
        $header  = 'MIME-Version: 1.0' . "\r\n";
        $header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // zusätzliche Header
        // $header .= 'To: Artur <' . $empfaenger . '>' . "\r\n";
        $header .= 'From: Dies Das <' . $info->email . '>' . "\r\n";
        // $header .= 'Cc: geburtstagsarchiv@example.com' . "\r\n";
        // $header .= 'Bcc: geburtstagscheck@example.com' . "\r\n";

        // verschicke die E-Mail
        if (mail($empfaenger, $betreff, $nachricht, $header)) {
        return back();
        }else{

        return redirect()->route('home');
        }
    }
    public function guestbook(Request $request)
    {
        $entry = Guestbook::create([
            'nick'   => $request->nick,
            'body'   => $request->body,
            'avatar'   => $request->avatar,
            'status' => 'new',

        ]);
        return back();
    }
}
