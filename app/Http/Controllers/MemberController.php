<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use Session;
use Validator;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create()
    {
        $members = User::where('type', 1)->get();
        return view('backend.index.member', compact('members'));
    }

    public function store(Request $request)
    {

        $file = array('pic' => $request->file('pic'));
        $rules = array(
            'pic' => 'required|mimes:jpg,jpeg,bmp,png|max:4500',
            );
        $validator = Validator::make($file, $rules);
        
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }else{
            $destinationPath = 'img/users'; // upload path
            $extension = $request->file('pic')->getClientOriginalExtension(); // getting image extension
            $fileName = 'member_'.rand(11111,99999).'.'.$extension; // renameing image

            $request->file('pic')->move($destinationPath, $fileName); // uploading file to given path
            User::create([
                'name'         => $request->name,
                'member_email' => $request->member_email,
                'title'        => $request->title,
                'keywords'     => $request->keywords,
                'type'         => 1,
                'pic'          => '/'. $destinationPath .'/'. $fileName,
                'facebook'     => $request->facebook,
                'twitter'      => $request->twitter,
                'googleplus'   => $request->googleplus,
                'linkedIn'     => $request->linkedIn,
            ]);
            return back();
        }

    }

    public function show(User $user)
    {
        //
    }

    public function edit(User $user, $id)
    {
        if ($id == 1 || $id == 2 || Auth::id() >= 2 || empty(User::find($id))) {
            return back();
        }
        $member = $user->find($id);
        $members = User::where('type', 1)->get();
        return view('backend.member.edit', compact('member', 'members'));
    }

    public function update(Request $request, User $member)
    {
        $destinationPath = 'img/users'; // upload path
        $fileName = NULL;
        $file = array('pic' => $request->file('pic'));
        $rules = array(
            'pic' => 'mimes:jpg,jpeg,png|max:4500',
            );
        $validator = Validator::make($file, $rules);
        
        if (!empty($request->file('pic')) && $validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }else{


            if (!empty($request->file('pic'))) {
                if (file_exists(public_path($member->pic))) {
                    unlink(public_path($member->pic));
                }

                $member->update($request->all());
                $extension = $request->file('pic')->getClientOriginalExtension(); // getting image extension
                $fileName = 'member_'.rand(11111,99999).'.'.$extension; // renameing image
                $request->file('pic')->move($destinationPath, $fileName); // uploading file to given path
                $member->update([
                    'pic'            => '/'. $destinationPath .'/'. $fileName,
                ]);
            }else{
                $member->update([
                    'name'         => $request->name,
                    'member_email' => $request->member_email,
                    'title'        => $request->title,
                    'facebook'     => $request->facebook,
                    'twitter'      => $request->twitter,
                    'googleplus'   => $request->googleplus,
                ]);
            }

            
        }
        return back();
    }

    public function destroy(User $member)
    {
        if ($member->type == 1 && Auth::user()->type == 2 || Auth::user()->type == 3 ) {
            unlink(public_path($member->pic));
            $member->delete();
        }
        return redirect()->route('Member');
    }
}
