<?php

namespace App\Http\Controllers;

use App\Models\Guestbook;
use Illuminate\Http\Request;

class GuestbookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(Guestbook $guestbook)
    {
        $guestbooks = Guestbook::all();
        return view('backend.index.guestbook', compact('guestbooks'));
    }

    public function update(Request $request, Guestbook $guestbook)
    {
        $guestbook = $guestbook->update([
            'status' =>  $request->status,
        ]);

        if ($guestbook) {
            return 'true';
        }else{
            return 'false';
        }
        
    }
    public function destroy(Guestbook $guestbook)
    {
        $guestbook->delete();
        return back();
    }
}
