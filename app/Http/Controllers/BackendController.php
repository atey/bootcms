<?php

namespace App\Http\Controllers;

use App\Models\Configuration;
use App\Models\ConfigurationUser;
use App\Models\Section;
use App\Models\Guestbook;

use Auth;

use Illuminate\Http\Request;

class BackendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $newguestbook = Guestbook::where('status',  'new')->count();
        return view('backend.index', compact('newguestbook'));
    }
    public function faqs()
    {
        $config = Configuration::find(1);
        return view('backend.faqs', compact('config'));
    }

    public function agbs()
    {
        $config = Configuration::find(1);
        return view('backend.index.agbs', compact('config'));
    }

    public function datenschutz()
    {
        $config = Configuration::find(1);
        return view('backend.index.datenschutz', compact('config'));
    }

    public function editAgbs(Request $request)
    {
        
        $config = new ConfigurationUser;
        $config->impressum = $request->impressum;
        $config->agb = $request->agbs;
        $config->save();

        return back();
    }

    public function getSection(Request $request)
    {

    	$section = Section::find($request->id);
    	$url = asset('views/layouts/sections/contact.blade.php');
    	$data['preview'] = file_get_contents($url);
    	$data['amount_heads'] = $section->amount_heads;

        return $data;
    }
}
