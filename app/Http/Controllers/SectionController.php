<?php

namespace App\Http\Controllers;

use App\Models\Section;

use Illuminate\Http\Request;

class SectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    public function create()
    {
        $sections = Section::all();
        return view('backend.section', compact('sections'));
    }

    public function store(Request $request)
    {
        Section::create([
            "title"             => $request->title,
            "amount_heads"      => $request->amount_heads,
            "amount_links"      => $request->amount_links,
            "title_text_active" => $request->title_text_active,
        ]);

        return back();
    }

    public function show(Section $section)
    {
        //
    }

    public function edit(Section $section)
    {
        //
    }

    public function update(Request $request, Section $section)
    {
        //
    }

    public function destroy(Section $section)
    {
        //
    }
}
