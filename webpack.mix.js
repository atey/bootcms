let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.scripts([
	    'resources/assets/js/template/jquery.min.js',
	    'resources/assets/js/template/jquery.easing.min.js',
	    'resources/assets/js/template/bootstrap.min.js',
	    'resources/assets/js/template/custom.js',
	    'resources/assets/js/slick.min.js',
	    'resources/assets/js/main.js',
	], 'public/js/all.js')
   .styles([
	    'resources/assets/css/template/font-awesome.min.css',
	    'resources/assets/css/template/bootstrap.min.css',
	    'resources/assets/css/template/style.css',
	    'resources/assets/css/slick.css',
	    'resources/assets/css/slick-theme.css',
	    'resources/assets/css/main.css',
	], 'public/css/all.css')
   .copyDirectory('resources/assets/fonts', 'public/fonts');