<section id="" class="cta-2 section-padding">
	<div class="container">
		<div class="row">
  			<div class="col-md-2"></div>
        <div class="text-right-md col-md-4 col-sm-4">
          <h2 class="section-title white lg-line">« Artikel Bereich Überschrift 1 »</h2>
        </div>
        <div class="col-md-4 col-sm-5">
          <b>Artikel Bereich Text 1</b> simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a typek 
          <p class="text-right text-primary"><i>— >>Hier steht bereits dein Firmenname << </i></p>
        </div>
        <div class="col-md-2"></div>
      </div>
	</div>
</section>
