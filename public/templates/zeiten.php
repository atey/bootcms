<section id="" class="section-padding">
	<div class="container">
		<div class="row">
			<div class="schedule-tab">
			<div class="col-md-4 col-sm-4 bor-left">
				<div class="mt-boxy-color"></div>
    			<div class="medi-info">
	    			<h3>Haupt-Überschrift</h3>
					<p><b>Text unter Haupt-Überschrift</b> Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="medi-info">
	    			<h3>Überschrift Bereich</h3>
					<p><b>Text unter Überschrift Bereich</b> Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 mt-boxy-3 text-center">
				<div class="mt-boxy-color"></div>
				<div class="time-info">
		    			<h3>Öffnungszeiten</h3>

		    			<table class="table table-responsive">
		    				<tbody>
							<tbody>
								<tr>
								<td>Montag - Freitag</td>
								<td>8.00 - 17.00</td>
								</tr>
								<tr>
								<td>Samstag</td>
								<td>9.30 - 17.30</td>
								</tr>
								<tr>
								<td>Sonntag</td>
								<td>9.30 - 15.00</td>
								</tr>
							</tbody>
		    				</tbody>
		    			</table>
		    			<div style="font-size: 10px;">
				    	<i class="fa fa-info-circle" aria-hidden="true"></i> Die Öffnungszeiten kannst Du an einer anderen Stelle bearbeiten
		    			</div>			    		
				</div>
			</div>
			</div>
		</div>
	</div>
</section>
