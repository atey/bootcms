<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('/', 'HomeController@home')->name('home');
Route::get('/impressum', 'HomeController@impressum');
Route::get('/agbs', 'HomeController@agbs');
Route::get('/datenschutz', 'HomeController@datenschutz');
Route::post('/send-message', 'HomeController@mail');
Route::post('/send-guestbook', 'HomeController@guestbook');

Route::get('/home', function (){
        return redirect()->route('home');
	})->name('backend');

Route::get('/backend', function (){
        return redirect()->route('admin');
	})->name('backend');

Route::get('/admin', 'BackendController@index')->name('admin');

Route::get('/backend/member/create', 	'MemberController@create')->name('Member');
Route::get('/backend/member/edit/{id}', 			'MemberController@edit');
Route::get('/backend/member/destroy/{member}', 		'MemberController@destroy');
Route::post('/backend/member/store', 				'MemberController@store');
Route::patch('/backend/member/update/{member}', 	'MemberController@update');

//		--- Sektionen / Artikel anlegen ---
Route::get('/backend/article/create', 					'ArticleController@create')->name('Article');
Route::get('/backend/article/edit/{id}', 				'ArticleController@edit')->name('ArticleEdit');
Route::get('/backend/article/edit/{id}/{position}', 	'ArticleController@changePosition');
Route::get('/backend/article/delete/{article}', 		'ArticleController@destroy');
Route::patch('/backend/article/update/{id}', 			'ArticleController@update');
Route::post('/backend/article/store', 					'ArticleController@store');

//		--- Öffnungzeiten ---
Route::get('/backend/opening/edit',			'ConfigurationController@opening')->name('Opening');
Route::get('/backend/opening/delete/{articleRecord}', 	'ArticleController@destroyOpening');
Route::post('/backend/opening/store',					'ArticleController@storeOpening');
Route::post('/backend/opening/update/{articleRecord}', 	'ArticleController@updateOpening');

//		--- Gäastebuch ---
Route::get('/backend/guestbook/edit',					'GuestbookController@edit')->name('Guestbook');
Route::get('/backend/guestbook/delete/{guestbook}', 	'GuestbookController@destroy');
Route::post('/backend/guestbook/update/{guestbook}', 	'GuestbookController@update');

//		--- Auth ---
Route::get('/backend/auth/edit',			'ConfigurationController@auth');
Route::post('/backend/auth/update', 		'ConfigurationController@updateAuth');

//		--- Allgemeine Infomrationen beabreiten ---
Route::get('/backend/webconfig/edit',		'ConfigurationController@webconfig');
Route::get('/backend/contact/edit',			'ConfigurationController@contact');
Route::get('/backend/head/edit',			'ConfigurationController@head');
Route::get('/backend/socialmedia/edit',		'ConfigurationController@socialmedia');

//		--- Allgemeine Infomrationen speichern ---
Route::post('/backend/config/store',		'ConfigurationController@updateHead');
Route::post('/backend/config/update',		'ConfigurationController@update');
Route::post('/backend/config/meta/update',	'ConfigurationController@updateMeta');
Route::post('/backend/config/logo/update',	'ConfigurationController@updateLogo');

Route::get('/backend/agbs', 'BackendController@agbs');
Route::get('/backend/datenschutz', 'BackendController@datenschutz');
Route::post('/backend/agbs/update', 	'ConfigurationController@updateAgbs');
Route::post('/backend/datenschutz/update', 	'ConfigurationController@updateDatenschutz');

Route::get('/backend/faqs', 'BackendController@faqs');

Route::post('/backend/get/section', 'BackendController@getSection');

//		--- Admin-Backend ---
Route::get('/backend/section/create', 'SectionController@create');
Route::post('/backend/section/store', 'SectionController@store');